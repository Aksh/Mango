#include <set>
#include <iostream>
#include <iterator>
#include <algorithm>

int main()
{
	const int N = 10;
	int a[N] = {4, 1, 1, 1, 1, 1, 0, 5, 1, 0};
	int b[N] = {4, 4, 2, 4, 2, 4, 0, 1, 5, 5};

	std::multiset<int> A(a, a + N);
	std::multiset<int> B(b, b + N);
	std::multiset<int> C;

	std::cout << "Set A : ";
	std::copy(A.begin(), A.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;

	std::cout << "Set B : ";
	std::copy(B.begin(), B.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;

	std::cout << "Union : ";
	std::set_union(A.begin(), A.end(), B.begin(), B.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;

	std::cout << "Intersection : ";
	std::set_intersection(A.begin(), A.end(), B.begin(), B.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;

	std::set_difference(A.begin(), A.end(), B.begin(), B.end(), std::inserter(C, C.end()));
	//std::set_difference(B.begin(), B.end(), A.begin(), A.end(), std::inserter(C, C.end()));
	std::cout << "Set C (difference of A and B) : ";
	std::copy(C.begin(), C.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;

	return 0;
}
