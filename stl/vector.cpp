#include <vector>
#include <string>
#include <iostream>

int main(int argc, char** argv)
{
	std::vector<std::string> args;
	args.push_back(argv[0]);
	for (int i = 1; i < argc; i++) {
		args.push_back(argv[i]);
	}

	for (std::vector<std::string>::iterator it = args.begin();
		it != args.end();
		it++)
		std::cout << *it << " ";
	std::cout << std::endl;
	return 0;
}
