#include <iostream>
#include <algorithm>
#include <iterator>
#include <fstream>

int main()
{
	std::ifstream ifile("input.txt");
	std::copy(std::istream_iterator<int> (ifile),
			  std::istream_iterator<int> (),
			  std::ostream_iterator<int> (std::cout, " "));
	
	std::cout << std::endl;
	return 0;
}
