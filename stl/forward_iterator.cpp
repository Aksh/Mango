#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <assert.h>

template <typename ForwardIterator, typename T>
void rplce(ForwardIterator first, ForwardIterator last, const T& old_value, const T& new_value)
{
	for ( ; first != last; ++first) {
		if (*first == old_value)
			*first = new_value;
	}
}

int main()
{
	std::vector<int> v(3, 1);
	v.push_back(7);
	std::copy(v.begin(), v.end(), std::ostream_iterator<int> (std::cout, " "));
	std::cout << std::endl;
	rplce(v.begin(), v.end(), 7, 1);
	std::copy(v.begin(), v.end(), std::ostream_iterator<int> (std::cout, " "));
	std::cout << std::endl;
	assert (std::find (v.begin(), v.end(), 7) == v.end());
	return 0;
}
