#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>

int main()
{
	std::vector<int> v;

	//for (std::istream_iterator<int> i = (std::cin);
	//	i != std::istream_iterator<int>();
	//	i++)
	//	v.push_back(*i);
	//std::cout << "Size : " << v.size() << std::endl;

	std::copy(std::istream_iterator<int>(std::cin),
			  std::istream_iterator<int>(),
			  std::back_inserter(v));
	std::cout << "Size : " << v.size() << std::endl;

	return 0;
}
