#include <iostream>
#include <map>

int main()
{
	std::multimap<int, int> m;
	//std::map<int, int> m;
	m.insert(std::pair<int, int>(1, 1));
	m.insert(std::pair<int, int>(1, 2));
	m.insert(std::pair<int, int>(2, 3));
	m.insert(std::pair<int, int>(3, 4));

	for (std::multimap<int, int>::iterator it = m.begin();
		it != m.end();
		it++)
		std::cout << it->first << " : " << it->second << std::endl;
	return 0;
}
