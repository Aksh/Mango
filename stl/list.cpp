#include <iostream>
#include <list>
#include <string>
#include <iterator>
#include <algorithm>

int main()
{
	std::list<std::string> a_list;
	a_list.push_back("banana");
	a_list.push_front("apple");
	a_list.push_back("carrot");
	std::ostream_iterator<std::string> out_it(std::cout, " ");
	std::copy(a_list.begin(), a_list.end(), out_it);
	std::cout << std::endl;
	std::reverse_copy(a_list.begin(), a_list.end(), out_it);
	std::cout << std::endl;
	std::copy(a_list.rbegin(), a_list.rend(), out_it);
	std::cout << std::endl;
	return 0;
}
