#include <iostream>
#include <deque>
#include <algorithm>
#include <iterator>

int main()
{
	std::deque<int> a_deck;
	a_deck.push_back(3);
	a_deck.push_front(1);
	a_deck.insert(a_deck.begin() + 1, 2);
	a_deck[2] = 0;
	std::copy(a_deck.begin(), a_deck.end(), std::ostream_iterator<int>(std::cout, " "));
	/*for (std::deque<int>::iterator it = a_deck.begin();
		it != a_deck.end();
		it++)
		std::cout << *it << " ";*/
	std::cout << std::endl;
	return 0;
}
