#include <iostream>
#include <string>
#include <tuple>
#include <map>

int main()
{
	std::tuple<int, std::string, std::string> t1;
	t1 = std::make_tuple(1, "Akshay", "Patil");
	int id;
	std::string n1, n2;
	std::tie(id, n1, n2) = t1;
	std::cout << id << " : " << n1 << " " << n2 << std::endl;
	std::map<int, int> m;
	if (m.find(1) == m.end()) std::cout << "1 is not present in map\n";
	std::cout << m[1] << std::endl;
	if (m.find(1) == m.end()) std::cout << "1 is not present in map\n";
	
	return 0;
}
