#include <iostream>
#include <map>
#include <string>
#include <algorithm>

typedef std::map<std::string, int> MyMap;

struct print {
	void operator () (const MyMap::value_type& p) {
		std::cout << p.second << " " << p.first << std::endl;
	}
};

int main()
{
	MyMap my_map;
	for (std::string a_word; std::cin >> a_word;)
		my_map[a_word]++;
	std::for_each(my_map.begin(), my_map.end(), print());

	return 0;
}
