#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cstring>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <pthread.h>

void* handle_request(void* args)
{
	//int fd = *(reinterpret_cast<int *>(args));
	int fd = *((int *)args);
	delete (int*)args;
	char buf[1024];
	while (1) {
		memset(buf, 0, 1024);
		read(fd, buf, 1024);
		std::cout << buf << std::endl;
		int i = 0;
		while (buf[i]) {
			buf[i] = toupper(buf[i]);
			i++;
		}

		write(fd, buf, i);
	}
}

int main(int argc, char** argv)
{
	struct sockaddr_in addr;
	int listenfd = socket(AF_INET, SOCK_STREAM, 0);
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(atoi(argv[1]));

	if(-1 == bind(listenfd, (struct sockaddr *)&addr, sizeof(addr))) {
		perror("bind: ");
	}
	listen(listenfd, 3);
	int len = sizeof(addr);
	int* fd;
	while (1) {
		fd = new int;
		*fd = accept(listenfd, (struct sockaddr *)&addr, (socklen_t*)&len);
		pthread_t tid;
		pthread_create(&tid, NULL, handle_request, fd);
	}

	return 0;
}
