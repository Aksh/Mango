#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <arpa/inet.h>

int main(int argc, char** argv)
{
	int fd;
	fd = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(atoi(argv[1]));
	inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr);
	connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	while (1) {
		char buf[1024] = {0};
		std::cin >> buf;
		write(fd, buf, strlen(buf));
		memset(buf, 0, 1024);
		read(fd, buf, 1024);
		std::cout << buf << std::endl;
	}

	return 0;
}
