/*
 * Restriction: it can only work with following parameters:
 * char const*
 * unsigned long long
 * long double
 * char const*, std::size_t
 * wchar_t const*, std::size_t
 * char16_t const*, std::size_t
 * char32_t const*, std::size_t
 *
 * Note: return value can be of any types.
 */

#include <iostream>

constexpr long double operator"" _cm(long double x) {return x * 10;}
constexpr long double operator"" _m(long double x) {return x * 1000;}
constexpr long double operator"" _mm(long double x) {return x;}

int main()
{
	long double height = 3.4_cm;
	std::cout << height << std::endl;
	std::cout << (height + 13.0_m) << std::endl;
	std::cout << (130.0_mm / 13.0_m) << std::endl;

	return 0;
}
