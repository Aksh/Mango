#include <iostream>

// Force computation to happen at compile time
constexpr int A(int i) {return i + i;}

int main()
{
	int arr[A(3) + 3];
	A(10); // computed at compile time

	return 0;
}
