#include <iostream>
#include <vector>

template <typename func>
void filter(func f, std::vector<int> arr)
{
	for (auto i: arr) {
		if (f(i))
			std::cout << i << " ";
	}

	std::cout << std::endl;
}

int main()
{
	std::cout << [](int x, int y) {return x + y;} (3, 4) << std::endl;	// output 7
	auto f = [](int x, int y) {return x * y;};
	std::cout << f(3, 4) << std::endl;	// output 12

	std::vector<int> v = {1, 2, 3, 4, 5, 6};
	filter([](int x){return x > 3;}, v);	// output 4 5 6

	int y = 4;
	int z = 2;
	filter([&](int x){return x > z && x < y;}, v);	// output 3
	//Note: [&] tells compiler that we want variable capture

	return 0;
}
