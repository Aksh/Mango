#include <iostream>

class demo {
public:
	int size;
	double* arr;
	
	demo() = default;

	demo& operator=(const demo&) = delete;

	demo(const demo& d) { // copy constructor
		std::cout << "demo(const demo&)\n";
		size = d.size;
		arr = new double[size];
		for (int i = 0; i < size; i++)
			arr[i] = d.arr[i];
	}
	
	demo(demo&& d) { // move constructor
		std::cout << "demo(demo&&)" << std::endl;
		size = d.size;
		arr = d.arr;
		d.arr = nullptr;
	}

	~demo() {
		std::cout << "~demo()\n";
		if (arr)
			delete []arr;
		arr = nullptr;
	}
};

void fun(demo d)
{
	std::cout << "fun(demo)\n";
	for (int i = 0; i < d.size; i++)
		std::cout << d.arr[i] << " ";
	
	std::cout << std::endl;
}

demo createDemo()
{
	demo d;
	d.size = 10;
	d.arr = new double[10];
	for (int i = 0; i < 10; i++)
		d.arr[i] = i;

	return d;
}

int main()
{
	demo d = createDemo();
	fun(d);
	
	fun(std::move(createDemo()));

	return 0;
}
