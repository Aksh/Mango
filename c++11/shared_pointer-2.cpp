#include <iostream>
#include <memory>

class demo {
	std::string name_;
public:
	demo() {
		std::cout << "Nameless demo created\n";
		name_ = "nameless";
	}

	demo(std::string name) {
		std::cout << "Demo is created : " << name << std::endl;
		name_ = name;
	}

	~demo() {
		std::cout << "Demo is destroyed : " << name_ << std::endl;
	}

	void fun() {
		std::cout << "Demo " << name_ << " rules!\n";
	}
};

void foo()
{
	std::shared_ptr<demo> p1 = std::make_shared<demo>("gunner"); // use default deleter: operator delete
	std::shared_ptr<demo> p2 = std::make_shared<demo>("tank");
	std::shared_ptr<demo> p3 = std::shared_ptr<demo>(new demo("abc"),
		[] (demo* p) {std::cout << "custom deleting. "; delete p;}
		);
	
	demo* d = p1.get(); // returns raw pointer! bad thing

	std::shared_ptr<demo> p4(new demo[3]); // demo[1] and demo[2] have memory leaks.
	std::shared_ptr<demo> p5(new demo[3], [](demo* p) {delete[] p;}); // all 3 demo will be deleted when p5 goes out of scope

	// gunner will be deleted if following things happen
	//p1 = p2;
	//p1 = nullptr;
	//p1.reset();
}

int main()
{
	foo();

	return 0;
}
