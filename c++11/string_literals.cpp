#include <iostream>

int main()
{
	const char* a = "akshay";
	const char* b = R"(akshay\\)"; // to define raw string
	const char* c = u8"akshay"; // to define UTF-8 string
	const char16_t* d = u"akshay"; // to define UTF-16 string
	const char32_t* e = U"akshay"; // to define UTF-32 string

	return 0;
}
