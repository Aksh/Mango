#include <iostream>

class base {
public:
	virtual void a(int) {}
	virtual void b() const {}
	void c() {}
};

class der:public base {
public:
	void a(float) override {} // marked override, but does not override
	void b() override {} // marked override, but does not override
	void c() override {} // marked override, but does not override
};

int main()
{
	return 0;
}
