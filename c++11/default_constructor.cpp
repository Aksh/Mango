#include <iostream>

class demo {
public:
	demo(int){}
	demo() = default; // Force compiler to generate default constructor
};

int main()
{
	demo d;

	return 0;
}
