#include <iostream>
#include <vector>

int main()
{
	std::vector<int> v = {5, 1, 4, 2, 3};

	for (const auto it : v)
		std::cout << it << " ";
	std::cout << std::endl;

	std::cout << "locally incremented\n";
	for (auto it : v)
		std::cout << ++it << " ";
	std::cout << std::endl;

	for (const auto it : v)
		std::cout << it << " ";
	std::cout << std::endl;

	std::cout << "reference incremented\n";
	for (auto& it : v)
		std::cout << ++it << " ";
	std::cout << std::endl;

	for (const auto it : v)
		std::cout << it << " ";
	std::cout << std::endl;

	return 0;
}
