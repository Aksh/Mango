/*
 * Regulat Expression: a regular expression is a specific pattern that provides concise and flexible means to "match" strings of text, such as perticular characters, words, or pattern of characters. - wikipedia
 */

#include <regex>
#include <iostream>
#include <string>

int main()
{
	std::string str;
	while (true) {
		std::cin >> str;
		//std::regex e("abc.", std::regex_constants::icase); // . Any character except newline
		//std::regex e("abc?"); // ? zero or one preceding character
		//std::regex e("abc*"); // * zero or more preceding character
		//std::regex e("abc+"); // + one or more preceding character
		//std::regex e("ab[cd]*"); // [...] Any character inside the square brackets can be match
		std::regex e("ab[^cd]*"); // [...] Any character not inside the square brackets can be match

		bool match = regex_match(str, e);
		std::cout << (match? "Matched" : "Not Matched") << std::endl;
	}

	return 0;
}
