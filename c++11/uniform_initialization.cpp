#include <initializer_list>
#include <iostream>
#include <string>

class demo {
public:
	int a;
	int b;

	demo(int a, int b) {
		std::cout << "regular constructor\n";
		this->a = a; this->b = b;
	}

	demo(const std::initializer_list<int>& v) {
		std::cout << "initializer list constructor\n";
		a = *(v.begin()); 
		b = *(v.begin() + 1);
	}
	void show()
	{
		std::cout << a << " " << b << std::endl;
	}
};

int main()
{
	demo d{25, 35};
	d.show();

	return 0;
}
