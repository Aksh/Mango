#include <iostream>

void fun(int a)
{
	std::cout << "fun(int)\n";
}

void fun(char* a)
{
	std::cout << "fun(char*)\n";
}

int main()
{
	fun(0);
	//fun(NULL); // this call is ambiguous
	fun(nullptr);
	return 0;
}
