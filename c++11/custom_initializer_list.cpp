#include <initializer_list>
#include <iostream>

class demo {
	int a[5];

public:
	demo(const std::initializer_list<int>& v) {
		int i = 0;
		for (std::initializer_list<int>::iterator it = v.begin(); it != v.end(); it++)
			a[i++] = *it;
	}

	void show()
	{
		for (int i = 0; i < 5; i++)
			std::cout << a[i] << " ";
		std::cout << std::endl;
	}
};

int main()
{
	demo d = {5, 1, 4, 2, 3};
	d.show();

	return 0;
}
