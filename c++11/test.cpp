#include <iostream>
#include <memory>

class demo {
	std::string name_;
public:
	//demo() {
	//	std::cout << "Nameless demo created\n";
	//	name_ = "nameless";
	//}

	//demo(std::string name) {
	//	std::cout << "Demo is created : " << name << std::endl;
	//	name_ = name;
	//}

	~demo() {
		std::cout << "Demo is destroyed : " << name_ << std::endl;
	}

	void fun() {
		std::cout << "Demo " << name_ << " rules!\n";
	}
};

void fun(demo d)
{
	std::cout << "fun called\n";
}

int main()
{
	demo d;
	fun(std::move(d));

	return 0;
}
