#include <iostream>
#include <memory>

class demo {
	std::string name_;
public:
	demo() {
		std::cout << "Nameless demo created\n";
		name_ = "nameless";
	}

	demo(std::string name) {
		std::cout << "Demo is created : " << name << std::endl;
		name_ = name;
	}

	~demo() {
		std::cout << "Demo is destroyed : " << name_ << std::endl;
	}

	void fun() {
		std::cout << "Demo " << name_ << " rules!\n";
	}
};

void foo()
{
	//demo* p = new demo("gunner");
	//// 1. gunner is created
	//// 2. p is created
	//// if step 1 successful and step 2 fails then it will result in memory leak
	//delete p;
	//p->fun(); // p is dangling pointer - undefined behavior
	
	std::shared_ptr<demo> p(new demo("gunner")); // count 1
	{
		std::shared_ptr<demo> p2 = p; // count 2
		p2->fun();
		std::cout << p.use_count() << std::endl;
	}
	// count 1
	p->fun();
} // count = 0;

int main()
{
	foo();
	
	// An object should be assigned to a smart pointer as soon as it is created. Raw pointer should not be used.
	demo* d = new demo("tank");
	std::shared_ptr<demo> p(d); // p.use_count == 1
	std::shared_ptr<demo> p2(d); // p2.use_count == 1

	std::shared_ptr<demo> p3 = std::make_shared<demo>("abc"); // faster and safer [exception safe]

	// we can cast shared pointer to other type like raw pointers
	// static_pointer_cast
	// dynamic_pointer_cast
	// const_pointer_cast

	return 0;
} // d will be destroyed twice which wil result undefined behavior
