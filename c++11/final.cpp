#include <iostream>

class d1 final {};

class d2 : public d1 {}; // error

class base {
public:
	virtual void fun() final;
};

class der : public base {
	void fun();	// error
};

int main()
{
	return 0;
}
