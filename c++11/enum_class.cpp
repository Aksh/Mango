#include <iostream>

enum class a{a1, a2};
enum class b{b1, b2};

enum class d1{d11};
enum class d2{d11};

// compiler error: redeclaration of z
//enum x{z};
//enum y{z};

int main()
{
	a o1 = a::a1;
	b o2 = b::b1;
	d1 o3 = d1::d11;

	// compiler error: cannot convert ‘d2’ to ‘d1’ in initialization
	//d1 o3 = d2::d11;

	// compile fails because we haven't defin ==(a, b)
	//if (o1 == o2)
	//	std::cout << "enum a == enum b\n";
	//else
	//	std::cout << "enum a != enum b\n";
	
	return 0;
}
