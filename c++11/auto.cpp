#include <iostream>
#include <vector>

int main()
{
	std::vector<int> v = {5, 1, 4, 2, 3};

	for (auto it = v.begin(); it != v.end(); it++)
		std::cout << *it << " ";
	std::cout << std::endl;

	return 0;
}
