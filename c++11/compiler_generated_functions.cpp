/*
 * C++98:
 * 1. default constructor (if no constructor is declared by user)
 * 2. copy constructor (if 3, 4, 5, 6 not declared by user)
 * 3. copy assignment operator (if 2, 4, 5, 6 not declared by user)
 * 4. destructor
 *
 * C++11:
 * 5. move constructor (if 2, 3, 4, 6 not declared by user)
 * 6. move assignment operator (if 2, 3, 4, 5 not declared by user)
 */

class demo {
	//C++98:
	demo(); // default constructor
	demo(const demo&); // copy constructor
	demo& operator=(const demo&); // copy assignment operator
	~demo(); // destructor

	//C++11:
	demo(demo&&); // move constructor
	demo& operator=(demo&&); // move assignment operator
};


class cat { // 3, 4
	cat(const cat&); // copy constructor
};

class duck { // 4
	duck(duck&&); // move constructor
};

class frog {
	frog(frog&&, int = 0); // move constructor
	frog(int = 0); // default constructor
	frog(const frog&, int = 0); // copy constructor
};

class fish { // 1, 2, 3 (2, 3 are depricated)
	~fish();
};

class cow { // 1, 2, 4 (2 is depricated)
	cow& operator=(const cow&) = delete;
}
