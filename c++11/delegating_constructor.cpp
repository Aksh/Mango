#include <iostream>

class demo {
public:
	demo() {
		std::cout << "demo::demo()\n";
	}
	demo(int) : demo() {
		std::cout << "demo::demo(int)\n";
	}
};

int main()
{
	demo d(11);
	return 0;
}
