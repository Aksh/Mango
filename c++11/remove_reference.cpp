#include <iostream>

template <class t>
struct remove_reference;

int main()
{
	int i = 10;
	int& j = i;
	
	remove_reference<int&>::type j = 20;
	
	std::cout << i << std::endl;

	return 0;
}
