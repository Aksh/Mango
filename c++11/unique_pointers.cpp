// Unique Pointers: exclusive ownership, light weight smart pointer

#include <iostream>
#include <memory>

class demo {
	std::string name_;
public:
	demo() {
		std::cout << "Nameless demo created\n";
		name_ = "nameless";
	}

	demo(std::string name) {
		std::cout << "Demo is created : " << name << std::endl;
		name_ = name;
	}

	~demo() {
		std::cout << "Demo is destroyed : " << name_ << std::endl;
	}

	void fun() {
		std::cout << "Demo " << name_ << " rules!\n";
	}
};

void test()
{
	std::unique_ptr<demo> pD(new demo("gunner"));

	pD->fun();
	/* pD does a bunch of different things */

	// returns raw pointer. pD has given ownership, nobody will automatically delete demo
	//demo* p = pD.release(); // pD will be empty
	//pD.reset(new demo("smokey"));
	pD.reset(); // pD = nullptr;
	if (!pD) {
		std::cout << "pD is empty\n";
	}
	else {
		std::cout << "pD is not empty\n";
	}
}

int main()
{
	//test();

	std::unique_ptr<demo> pD(new demo("gunner"));
	std::unique_ptr<demo> pD2(new demo("smokey"));
	pD2->fun();
	pD2 = std::move(pD);
	// 1. smokey is destroyed.
	// 2. pD becomes empty.
	// 3. pD2 owns gunner.
	pD2->fun();

	std::unique_ptr<demo[]> demos(new demo[3]); // no need to register custom deleter unlike shared_ptr

	return 0;
}
