/*
 * C++98/03 solutions:
 * 1. define copy constructor and copy assignment operator
 * 2. delete copy constructor and copy assignment operator
 *
 * C++11 solution:
 * 1. keywork 'delete'
 * 2. emplace_back()
 * 3. smartpointer [shared/unique] (we dont need to have destructor in that case)
 * 4. move()
 */

#include <iostream>
#include <vector>
#include <string>
#include <memory>

class demo {
public:
	demo(std::string name) : name_(new std::string(name)) {}
	demo(demo&& d) {name_ = d.name_; d.name_ = nullptr;}
	~demo() {delete name_;}
	void printName() {std::cout << *name_ << std::endl;}
private:
	std::string* name_;
	//std::shared_ptr<std::string> name_;
	//std::unique_ptr<std::string> name_;
};

int main()
{
	std::vector<demo> demos;
	//demos.push_back(demo("george")); // crash the program when access this member
	demos.push_back(std::move(demo("george"))); // crash the program when access this member
	//demos.emplace_back("george"); // construct the object in place(in the space allocated to the vector)
	demos.front().printName();

	return 0;
}
