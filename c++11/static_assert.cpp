#include <iostream>

int main()
{
	// compile time assert
	static_assert(sizeof(int) != 4, "int size is 4");
}
