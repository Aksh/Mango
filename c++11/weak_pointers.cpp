#include <iostream>
#include <memory>

// weak_ptr has no ownership of the pointed object

class demo {
	//std::shared_ptr<demo> m_pFriend; // cyclic reference - destructor will not be called for gunner and smokey
	std::weak_ptr<demo> m_pFriend;
public:
	std::string m_name;
	void fun() {std::cout << "demo " << m_name << "rules!\n";}
	demo(std::string name) {std::cout << "Demo is created : " << name << std::endl; m_name = name;}
	~demo() {std::cout << "demo is destroyed: " << m_name << std::endl;}
	void makeFriend(std::shared_ptr<demo> f) {m_pFriend = f;}
	// weak_ptr.lock() creates shared pointer out of the weak pointer. Why?
	// To make sure that 2 things
	// 1. it check if the weak pointer is still points to valid object
	// 2. it make sure while accessing object, that object is not deleted.
	// lock function throws exception if weak_ptr is empty
	// we can check that with expired() function
	void showFriend() {
		if (!m_pFriend.expired()) {
			std::cout << "My friend is : " << m_pFriend.lock()->m_name << std::endl;
			std::cout << "It is owned by " << m_pFriend.use_count() << " pointers\n";
		}
	}
};

int main()
{
	std::shared_ptr<demo> pD(new demo("gunner"));
	std::shared_ptr<demo> pD2(new demo("smokey"));
	pD->makeFriend(pD2);
	pD2->makeFriend(pD);
	pD->showFriend();

	return 0;
}
