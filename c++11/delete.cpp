#include <iostream>

class demo {
public:
	void fun(double) = delete;
	demo& operator=(const demo&) = delete;
	void fun(int a) {std::cout << "fun\n";}
};

int main()
{
	demo d;
	d.fun(2);
	d.fun(3.0); // error: use of deleted function
	demo a;
	a = d; // error: use of deleted function

	return 0;
}
