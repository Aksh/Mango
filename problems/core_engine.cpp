#include "core_engine.h"

vector<FileAndData*> g_Documents;
pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;


FileAndData::FileAndData(string& n, string& c) : name(n), content(c) {}

string FileAndData::getName() {return name;}

size_t FileAndData::count(string str)
{
	size_t _count = 0;
	size_t nPos = this->content.find(str, 0);
	while (nPos != string::npos) {
		_count++;
		nPos = this->content.find(str, nPos + 1);
	}

	return _count;
}

bool FileAndData::search(string str)
{
	return string::npos != this->content.find(str, 0);
}

void* facet_thread(void* args)
{
	argsStruct* fStruct = reinterpret_cast<argsStruct*>(args);
	size_t _count = fStruct->oPtr->count(fStruct->str);
	if (0 != _count) {
		fStruct->_count = _count;
		return fStruct;
	}

	return NULL;
}

void* search_thread(void* args)
{
	argsStruct* sStruct = reinterpret_cast<argsStruct*>(args);
	if (sStruct->oPtr->search(sStruct->str))
		return sStruct;
	return NULL;
}

void add(string& name, string& content)
{
	FileAndData* fd = new FileAndData(name, content);
	pthread_mutex_lock(&g_mutex);
	g_Documents.push_back(fd);
	pthread_mutex_unlock(&g_mutex);
}

void del(string& name)
{
	pthread_mutex_lock(&g_mutex);
	vector<FileAndData*>::iterator it = g_Documents.begin();
	for (; it != g_Documents.end() && name != (*it)->getName(); it++);
	if (it != g_Documents.end())
		g_Documents.erase(it);
	pthread_mutex_unlock(&g_mutex);
}

vector<FileAndData*>& getDocuments()
{
	return g_Documents;
}

void search(string& str)
{
	vector<pthread_t> vPid;
	pthread_t pid;
	pthread_mutex_lock(&g_mutex);
	for (vector<FileAndData*>::iterator it = g_Documents.begin();
		it != g_Documents.end();
		it++) {
		argsStruct* ss = new argsStruct();
		ss->oPtr = *it;
		ss->str = str;
		pthread_attr_t attr;
		
		pthread_create(&pid, NULL, search_thread, ss);
		vPid.push_back(pid);
	}
	pthread_mutex_unlock(&g_mutex);

	stringstream sstr;

	for (vector<pthread_t>::iterator it = vPid.begin();
		it != vPid.end();
		it++) {
		void* vptr = NULL;
		pthread_join(*it, &vptr);
		if (vptr) {
			sstr << (reinterpret_cast<argsStruct*>(vptr))->oPtr->getName() << ", ";
		}
	}

	if (!sstr.str().empty()) {
		string result = sstr.str();
		result[result.length() - 2] = '\0';
		cout << result << endl;
	}
}

void facet(string& str)
{
	vector<pthread_t> vPid;
	pthread_t pid;
	pthread_mutex_lock(&g_mutex);
	for (vector<FileAndData*>::iterator it = g_Documents.begin();
		it != g_Documents.end();
		it++) {
		argsStruct* ss = new argsStruct();
		ss->oPtr = *it;
		ss->str = str;
		pthread_attr_t attr;
		
		pthread_create(&pid, NULL, facet_thread, ss);
		vPid.push_back(pid);
	}
	pthread_mutex_unlock(&g_mutex);

	stringstream sstr;

	for (vector<pthread_t>::iterator it = vPid.begin();
		it != vPid.end();
		it++) {
		void* vptr = NULL;
		pthread_join(*it, &vptr);
		if (vptr) {
			sstr << (reinterpret_cast<argsStruct*>(vptr))->oPtr->getName() << " ";
			sstr << (reinterpret_cast<argsStruct*>(vptr))->_count << ", ";
		}
	}

	if (!sstr.str().empty()) {
		string result = sstr.str();
		result[result.length() - 2] = '\0';
		cout << result << endl;
	}
}
