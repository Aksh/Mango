#ifndef _HCORE_ENGINE__
#define _HCORE_ENGINE__

#include <string>
#include <vector>
#include <sstream>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <cstdlib>

using namespace std;

class FileAndData {
public:
	FileAndData(string&, string&);
	size_t count(string);
	bool search(string);
	string getName();

//private:
	string name;
	string content;
};

struct argsStruct {
	string str;
	FileAndData* oPtr;
	size_t _count;
};

void* facet_thread(void*);
void* search_thread(void*);
void add(string&, string&);
void del(string&);
void search(string&);
void facet(string&);
vector<FileAndData*>& getDocuments();

#endif
