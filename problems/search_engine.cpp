#include "core_engine.h"
#include <iostream>
#include <sstream>
#include <cstdio>

void _add()
{
	string name;
	cout << "Document name : ";
	cin >> name;
	cout << "content : ";
	stringstream ss;
	string ch;
	cin.clear();
	while (getline(cin, ch)) {
		ss << ch << endl;
	}
	
	cin.clear();
	string content = ss.str();
	add(name, content);
}

void _del()
{
	string name;
	cout << "Document name : ";
	cin >> name;
	del(name);
}

void _search()
{
	string str;
	cout << "string to be search : ";
	cin >> str;
	search(str);
}

void _facet()
{
	string str;
	cout << "string to be search : ";
	cin >> str;
	facet(str);
}

int main()
{
	string ch;
	while (true) {
		cout << "$ ";
		cin >> ch;
		if ("add" == ch) {
			_add();
		}
		else if ("del" == ch) {
			_del();
		}
		else if ("search" == ch) {
			_search();
		}
		else if ("facet" == ch) {
			_facet();
		}
		else if ("exit" == ch) {
			break;
		}
		else {
			cout << "invalid command\n";
			cout << "<add|del|search|facet|exit>\n\n";
		}
	}

	return 0;
}
