#include <iostream>
#include <vector>

std::vector<int> adj[5];

bool hasroot(int v, int u, bool visited[])
{
	visited[v] = true;
	//std::cout << v << " ";
	for (auto it = adj[v].begin(); it != adj[v].end(); it++) {
		if (false == visited[*it]) {
			if (*it == u) return true;
			return hasroot(*it, u, visited);
		}
	}

	return false;
}

int main()
{
	bool visited[5] = {false, false, false, false, false};
	//bool visited[5] = {false};
	adj[0].push_back(1);
	adj[1].push_back(3);
	adj[2].push_back(1);
	adj[3].push_back(2);
	adj[4].push_back(3);

	if (hasroot(0, 3, visited))
		std::cout << "0 has root to 3\n";
	else
		std::cout << "no root between 0 and 3\n";

	return 0;
}
