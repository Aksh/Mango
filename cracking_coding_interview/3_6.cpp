#include <iostream>
#include <stack>

std::stack<int> sort(std::stack<int> s)
{
	int n = s.size();
	std::stack<int>* s1 = new std::stack<int>();
	std::stack<int>* s2 = &s;
	std::stack<int> res;
	while (n > res.size()) {
		int min = s2->top();
		s2->pop();
		while (!s2->empty()) {
			if (s2->top() > min) {
				s1->push(s2->top());
			}
			else {
				s1->push(min);
				min = s2->top();
			}

			s2->pop();
		}

		res.push(min);
		std::stack<int>* t = s1;
		s1 = s2;
		s2 = t;
	}

	return res;
}

int main()
{
	std::stack<int> s;
	s.push(99);
	s.push(11);
	s.push(88);
	s.push(22);
	s.push(77);
	s.push(33);
	s.push(66);
	s.push(44);
	s.push(55);

	s = sort(s);
	while (!s.empty()) {
		std::cout << s.top() << " ";
		s.pop();
	}

	std::cout << std::endl;

	return 0;
}
