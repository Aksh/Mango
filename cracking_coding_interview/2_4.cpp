#include <iostream>

struct node {
	int data;
	struct node* next;
};

void insert(struct node** head, int data)
{
	if (NULL == *head) {
		*head = new node;
		(*head)->data = data;
		(*head)->next = NULL;
		return;
	}

	node* temp = *head;
	while (temp->next)
		temp = temp->next;
	temp->next = new node;
	temp->next->data = data;
	temp->next->next = NULL;
}

void print(node* first)
{
	while (first) {
		std::cout << first->data << " ";
		first = first->next;
	}

	std::cout << std::endl;
}

node* add(node* one, node* two)
{
	node* head = NULL;
	node* last = NULL;
	int carry = 0;

	while (one && two) {
		if (NULL == head) {
			head = new node;
			head->data = (one->data + two->data) % 10;
			head->next = NULL;
			carry = (one->data + two->data) / 10;
			last = head;
		}
		else {
			last->next = new node;
			last->next->data = (carry + one->data + two->data) % 10;
			last->next->next = NULL;
			carry = (carry + one->data + two->data) / 10;
			last = last->next;
		}

		one = one->next;
		two = two->next;
	}

	node* rem = NULL;
	if (one) rem = one;
	if (two) rem = two;
	while (rem) {
		last->next = new node;
		last->next->data = (carry + rem->data) % 10;
		last->next->next = NULL;
		carry = (carry + rem->data) / 10;
		last = last->next;
		rem = rem->next;
	}

	if (carry) {
		last->next = new node;
		last->next->data = carry;
		last->next->next = NULL;
	}

	return head;
}

int main()
{
	node* one = NULL;
	node* two = NULL;
	insert(&one, 3);
	insert(&one, 1);
	insert(&one, 5);
	insert(&two, 5);
	insert(&two, 9);
	insert(&two, 4);
	insert(&two, 9);

	print(one);
	std::cout << "+\n";
	print(two);
	std::cout << "===================\n";

	node* result = add(one, two);
	print (result);

	return 0;
}
