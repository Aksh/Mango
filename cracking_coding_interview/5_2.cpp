#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stack>

void print_dec_bin(int left)
{
	std::stack<int> s;
	while (left) {
		s.push(left % 2);
		left /= 2;
	}

	while (!s.empty()) {
		std::cout << s.top();
		s.pop();
	}
}

void print_fp_bin(double right)
{
	std::cout << '.';
	while (right) {
		std::cout << ((int)(right * 2));
		right = (right * 2) - (int)(right * 2);
	}
}

int main(int argc, char** argv)
{
	int left = 0;
	int i = 0;
	int len = strlen(argv[1]);
	while (argv[1][i] != '.') {
		left = (left * 10) + (argv[1][i] - 48);
		i++;
	}

	i++;
	int fp = 1;
	float right = 0.0;
	while (argv[1][i] != '\0') {
		right = right + ((double)(argv[1][i] - 48) / (fp * 10));
		i++;
		fp *= 10;
	}
	
	print_dec_bin(left);
	print_fp_bin(right);
	std::cout << std::endl;
	return 0;
}
