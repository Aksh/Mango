#include <iostream>

int main()
{
	int n;
	std::cin >> n;
	int b = 0;
	while ((1 << b) & n)
		b++;
	
	int l = b;
	while (0 == ((1 << b) & n))
		b++;

	int small = n;
	small ^= (1 << l);
	small ^= (1 << b);
	std::cout << small << std::endl;

	int large = n;
	b = 0;
	while (0 == ((1 << b) & n))
		b++;
	
	l = b;
	while ((1 << b) & n)
		b++;

	large ^= (1 << l);
	large ^= (1 << b);
	std::cout << large << std::endl;
}
