#include <iostream>
#include <vector>
#include <cstring>
//#include <cstdio>
#include <cstdlib>

void insert(char** s, char c, int pos)
{
	*s = (char *) realloc(*s, strlen(*s) + 2);
	int len = strlen(*s);
	//std::cout << "insert::" << *s << "::" << len << "::" << pos << std::endl;
	int i = len + 1;
	for (; i > pos; i--)
		(*s)[i] = (*s)[i - 1];
	(*s)[i] = c;
}

std::vector<char*> perm(char* s)
{
	if (1 == strlen(s)) {
		std::vector<char *> v;
		v.push_back(s);
		return v;
	}

	char ch = s[0];
	std::vector<char* > v = perm(&s[1]);
	for (auto it = v.begin(); it != v.end(); it++) {
		int len = strlen(*it) + 1;
		while (len) {
			insert(&(*it), ch, len);
			len--;
		}
	}

	return v;
}

int main(int argc, char** argv)
{
	char* s = (char *)malloc(strlen(argv[1]) + 1);
	strcpy(s, argv[1]);
	//std::cout << s << std::endl;
	//std::cout << argv[1] << std::endl;
	//insert(&s, 'a', strlen(s));
	//std::cout << s << std::endl;
	std::vector<char *> p = perm(s);

	for (auto it = p.begin(); it != p.end(); it++)
		std::cout << *it << " ";
	
	std::cout << std::endl;

	return 0;
}
