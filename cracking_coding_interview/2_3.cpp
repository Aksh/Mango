#include <iostream>

struct node {
	int data;
	struct node* next;
};

void insert(struct node** head, int data)
{
	if (NULL == *head) {
		*head = new node;
		(*head)->data = data;
		(*head)->next = NULL;
		return;
	}

	node* temp = *head;
	while (temp->next)
		temp = temp->next;
	temp->next = new node;
	temp->next->data = data;
	temp->next->next = NULL;
}

void del(struct node* victim)
{
	node* tmp = victim->next;
	victim->data = tmp->data;
	victim->next = tmp->next;
	delete tmp;
}

void print(node* first)
{
	while (first) {
		std::cout << first->data << " ";
		first = first->next;
	}

	std::cout << std::endl;
}

int main()
{
	node* head = NULL;
	insert(&head, 77);
	insert(&head, 11);
	insert(&head, 66);
	insert(&head, 22);
	insert(&head, 33);
	insert(&head, 55);
	insert(&head, 44);

	node* tmp = head->next->next->next->next;

	print(head);
	std::cout << "delete " << tmp->data << std::endl;
	del(tmp);
	print(head);
	std::cout << "delete " << head->data << std::endl;
	del(head);
	print(head);

	return 0;
}
