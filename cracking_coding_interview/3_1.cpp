#include <iostream>

int stack[10];
int top1 = 0;
int top2 = 9;

void push(int s, int a)
{
	if (top1 == top2) {
		std::cout << "No space\n";
		return;
	}
	
	((s == 1) ? (stack[top1++] = a) : (stack[top2--] = a));
}

int pop(int s)
{
	if (((s == 1) && (0 == top1)) || ((s == 2) && (9 == top2))) {
		return -1;
	}

	return ((s == 1) ? (stack[--top1]) : (stack[++top2]));
}

void show()
{
	std::cout << "Stack 1 : ";
	for (int i = top1 - 1; i >= 0; i--)
		std::cout << stack[i] << " ";
	std::cout << std::endl;
	std::cout << "Stack 2 : ";
	for (int i = top2 + 1; i < 10; i++)
		std::cout << stack[i] << " ";
	std::cout << std::endl;
}

int main()
{
	int ch, n, s;
	do {
		std::cout << "1. push\n2. pop\n3. show\n4. exit\n";
		std::cin >> ch;
		switch (ch) {
			case 1:
				std::cin >> s >> n;
				push(s, n);
				break;
			case 2:
				std::cin >> s;
				std::cout << pop(s) << std::endl;
				break;
			case 3:
				show();
				break;
		}
	} while (ch != 4);
	
	return 0;
}
