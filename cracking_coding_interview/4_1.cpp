#include <iostream>

struct node {
	int data;
	node* left;
	node* right;
};

void insert(node** root, int data)
{
	if (NULL == *root) {
		*root = new node;
		(*root)->left = (*root)->right = NULL;
		(*root)->data = data;
		return;
	}

	if ((*root)->data < data)
		return insert(&((*root)->right), data);
	else
		return insert(&((*root)->left), data);
}

void inorder(node* root)
{
	if (root) {
		inorder(root->left);
		std::cout << root->data << " ";
		inorder(root->right);
	}
}

inline int max(int a, int b)
{
	return a > b ? a : b;
}

inline int min(int a, int b)
{
	return a > b ? b : a;
}

int max_depth(node* root)
{
	if (NULL == root)
		return 0;
	return 1 + max(max_depth(root->left), max_depth(root->right));
}

int min_depth(node* root)
{
	if (NULL == root)
		return 0;
	return 1 + min(min_depth(root->left), min_depth(root->right));
}

bool isbalanced(node* root)
{
	int mx = max_depth(root);
	int mn = min_depth(root);
	return (mx == mn || mx == mn + 1) ? true : false;
}

int main()
{
	node* root = NULL;
	insert(&root, 50);
	insert(&root, 75);
	insert(&root, 70);
	insert(&root, 25);
	insert(&root, 30);
	insert(&root, 80);
	insert(&root, 20);
	insert(&root, 60);
	insert(&root, 40);
	insert(&root, 90);
	insert(&root, 10);

	//insert(&root, 10);
	//insert(&root, 20);
	//insert(&root, 30);
	//insert(&root, 40);
	//insert(&root, 50);
	//insert(&root, 60);
	//insert(&root, 70);
	//insert(&root, 80);
	//insert(&root, 90);

	inorder(root);
	std::cout << std::endl;
	std::cout << "Max depth : " << max_depth(root) << std::endl;
	std::cout << "Min depth : " << min_depth(root) << std::endl;
	std::cout << (isbalanced(root) ? "Balanced" : "Not Balanced") << std::endl;

	return 0;
}
