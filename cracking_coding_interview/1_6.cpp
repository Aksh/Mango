#include <iostream>

int main()
{
	int n;
	std::cin >> n;
	int** a;
	a = new int*[n];
	for (int i = 0; i < n; i++) {
		a[i] = new int[n];
		for (int j = 0; j < n; j++)
			std::cin >> a[i][j];
	}

	std::cout << "\n===============\n";
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			std::cout << a[i][j] << " ";
		std::cout << std::endl;
	}
	std::cout << "===============\n";

	//
	// Work on it
	//
	int t, q;
	for (int i = 0; i < n; i++) {
		t = a[i][n - (i + 1)];
		a[i][n - (i + 1)] = a[i][i];
		q = a[n - (i + 1)][n - (i + 1)];
		a[n - (i + 1)][n - (i + 1)] = t;
		t = a[n - (i + 1)][i];
		a[n - (i + 1)][i] = q;
		a[i][i] = t;
	}

	std::cout << "\n===============\n";
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			std::cout << a[i][j] << " ";
		std::cout << std::endl;
	}
	std::cout << "===============\n";

	for (int i = 0; i < n; i++) {
		
	}
	
	return 0;
}
