#include <iostream>
#include <cstdio>
#include <cstring>

void reverse(char* s)
{
	int i = 0;
	int j = strlen(s) - 1;
	char t;
	while (i < j) {
		t = s[i];
		s[i] = s[j];
		s[j] = t;
		i++;
		j--;
	}
}

int main()
{
	char ch[1000] = {0};
	int i = 0;
	char c;
	while (EOF != (c = getchar())) {
		ch[i++] = c;
	}
	
	std::cout << ch << std::endl;
	reverse(ch);
	std::cout << ch << std::endl;

	return 0;
}
