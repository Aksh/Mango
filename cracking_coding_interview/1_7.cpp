#include <iostream>
#include <vector>

int main()
{
	int m, n;
	std::cin >> m >> n;
	int** a = new int*[m];
	std::vector<int> row;
	std::vector<int> column;

	for (int i = 0; i < m; i++) {
		a[i] = new int[n];
		for (int j = 0; j < n; j++) {
			std::cin >> a[i][j];
			if (0 == a[i][j]) {
				row.push_back(i);
				column.push_back(j);
			}
		}
	}

	for (auto it = row.begin(); it != row.end(); it++) {
		for (int i = 0; i < n; i++) {
			a[*it][i] = 0;
		}
	}

	for (auto it = column.begin(); it != column.end(); it++)
		for (int i = 0; i < m; i++) {
			a[i][*it] = 0;
		}

	std::cout << "\n===============\n";
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++)
			std::cout << a[i][j] << " ";
		std::cout << std::endl;
	}
	std::cout << "===============\n";
	
	return 0;
}
