#include <iostream>
#include <cstdio>
#include <cstring>

bool is_anagram(char *s1, char* s2)
{
	char hit[256] = {0};
	int i = 0;
	while (s1[i]) {
		hit[s1[i]]++;
		i++;
	}

	i = 0;
	while (s2[i]) {
		if (0 == hit[s2[i]])
			return false;
		hit[s2[i]]--;
		i++;
	}

	for (int j = 0; j < 256; j++)
		if (hit[j]) return false;

	return true;
}

int main()
{
	char ch1[1000] = {0};
	char ch2[1000] = {0};
	int i = 0;
	
	std::cin >> ch1 >> ch2;

	if(is_anagram(ch1, ch2))
		std::cout << "Anagram\n";
	else
		std::cout << "Not Anagram\n";

	return 0;
}
