#include <iostream>
#include <cstdio>
#include <cstring>

void replace_space(char *s)
{
	int i = 0;
	int count = 0;
	int tail = strlen(s);
	while (s[i]) {
		if (' ' == s[i]) count++;
		i++;
	}

	int new_tail = tail + count * 2;
	for (; count; count--) {
		while (' ' != s[tail]) {
			s[new_tail--] = s[tail--];
		}
		s[new_tail--] = '0';
		s[new_tail--] = '2';
		s[new_tail--] = '%';
		tail--;
	}
}

int main()
{
	char ch1[1000] = {0};

	int i = 0;
	char c;
	while (EOF != (c = getchar())) {
		ch1[i++] = c;
	}

	replace_space(ch1);
	std::cout << ch1 << std::endl;

	return 0;
}
