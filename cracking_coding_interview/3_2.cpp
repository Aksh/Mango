#include <iostream>

inline int in_min(int a, int b)
{
	return a > b ? b : a;
}

struct node {
	int min;
	int data;
};

node stack[5];
int top = 0;

int get_min()
{
	if (top)
		return stack[top - 1].min;
	return 9999;
}

void push(int data)
{
	if (5 == top) {
		std::cout << "No Space\n";
		return;
	}

	stack[top].data = data;
	stack[top].min = in_min(data, get_min());
	top++;
}

int pop()
{
	if (top)
		return stack[--top].data;
	return -1;
}

void show()
{
	for (int i = top - 1; i >= 0; i--)
		std::cout << stack[i].data << " ";
	std::cout << std::endl;
}

int main()
{
	int ch, n, s;
	do {
		std::cout << "1. push\n2. pop\n3. min\n4. show\n5. exit\n";
		std::cin >> ch;
		switch (ch) {
			case 1:
				std::cin >> n;
				push(n);
				break;
			case 2:
				std::cout << pop() << std::endl;
				break;
			case 3:
				std::cout << get_min() << std::endl;
				break;
			case 4:
				show();
				break;
		}
	} while (ch != 5);
	
	return 0;
}
