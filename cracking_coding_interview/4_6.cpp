#include <iostream>
#include <cstdlib>
#include <vector>
#include <queue>

struct node {
	int data;
	node* left;
	node* right;
	node* parent;
};

void insert(node** root, int data)
{
	if (NULL == *root) {
		*root = new node;
		(*root)->parent = (*root)->left = (*root)->right = NULL;
		(*root)->data = data;
		return;
	}

	if ((*root)->data < data) {
		insert(&((*root)->right), data);
		(*root)->right->parent = *root;
	}
	else {
		insert(&((*root)->left), data);
		(*root)->left->parent = *root;
	}
}

void inorder(node* root)
{
	if (root) {
		inorder(root->left);
		std::cout << root->data << " ";
		inorder(root->right);
	}
}

node* getNode(node* head, int data)
{
	if (head->data == data)
		return head;
	if (head->data > data)
		return getNode(head->left, data);
	return getNode(head->right, data);
}

inline int max(int a, int b)
{
	return a > b ? a : b;
}

inline int min(int a, int b)
{
	return a > b ? b : a;
}

int get_depth(node* inode)
{
	int depth = 0;
	while (inode) {
		inode = inode->parent;
		depth++;
	}

	return depth;
}

node* getcp(node* o, node* t)
{
	int d1 = get_depth(o);
	int d2 = get_depth(t);

	if (d1 > d2) {
		while (d1 != d2) {
			o = o->parent;
			d1--;
		}
	}
	else {
		while (d1 != d2) {
			t = t->parent;
			d2--;
		}
	}

	while (o != t) {
		o = o->parent;
		t = t->parent;
	}

	return o;
}

int main(int argc, char** argv)
{
	node* root = NULL;
	insert(&root, 50);
	insert(&root, 75);
	insert(&root, 70);
	insert(&root, 25);
	insert(&root, 30);
	insert(&root, 80);
	insert(&root, 20);
	insert(&root, 60);
	insert(&root, 40);
	insert(&root, 90);
	insert(&root, 10);
	insert(&root, 22);
	insert(&root, 27);
	insert(&root, 72);
	insert(&root, 77);

	inorder(root);
	std::cout << std::endl;
	node* one = getNode(root, atoi(argv[1]));
	node* two = getNode(root, atoi(argv[2]));
	node* cp = getcp(one, two);
	std::cout << "common ancestor of " << one->data << " and " << two->data << " is " << cp->data << std::endl;

	return 0;
}
