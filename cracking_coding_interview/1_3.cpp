#include <iostream>
#include <cstdio>
#include <cstring>

void remove_dup(char *s)
{
	char hit[256] = {0};
	int tail = 0;
	int i = 0;
	while (s[i]) {
		if (0 == hit[s[i]]) {
			hit[s[i]] = 1;
			s[tail] = s[i];
			tail++;
		}
		i++;
	}

	s[tail] = 0;
}

int main()
{
	char ch[1000] = {0};
	int i = 0;
	char c;
	while (EOF != (c = getchar())) {
		ch[i++] = c;
	}
	
	std::cout << ch << std::endl;
	remove_dup(ch);
	std::cout << ch << std::endl;

	return 0;
}
