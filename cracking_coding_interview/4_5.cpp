#include <iostream>
#include <cstdlib>
#include <vector>
#include <queue>

struct node {
	int data;
	node* left;
	node* right;
	node* parent;
};

void insert(node** root, int data)
{
	if (NULL == *root) {
		*root = new node;
		(*root)->parent = (*root)->left = (*root)->right = NULL;
		(*root)->data = data;
		return;
	}

	if ((*root)->data < data) {
		insert(&((*root)->right), data);
		(*root)->right->parent = *root;
	}
	else {
		insert(&((*root)->left), data);
		(*root)->left->parent = *root;
	}
}

void inorder(node* root)
{
	if (root) {
		inorder(root->left);
		std::cout << root->data << " ";
		inorder(root->right);
	}
}

node* getsuccessor(node* head)
{
	if (head->right) {
		head = head->right;
		while (head->left) head = head->left;
		return head;
	}
	if (head->parent) {
		node* parent = head->parent;
		while (parent && parent->right == head) {
			head = parent;
			parent = parent->parent;
		}

		return parent;
	}
}

node* getNode(node* head, int data)
{
	if (head->data == data)
		return head;
	if (head->data > data)
		return getNode(head->left, data);
	return getNode(head->right, data);
}

int main(int argc, char** argv)
{
	node* root = NULL;
	insert(&root, 50);
	insert(&root, 75);
	insert(&root, 70);
	insert(&root, 25);
	insert(&root, 30);
	insert(&root, 80);
	insert(&root, 20);
	insert(&root, 60);
	insert(&root, 40);
	insert(&root, 90);
	insert(&root, 10);

	inorder(root);
	std::cout << std::endl;
	node* temp = getNode(root, atoi(argv[1]));
	node* succ = getsuccessor(temp);
	std::cout << "successor of " << temp->data << " is " << succ->data << std::endl;

	return 0;
}
