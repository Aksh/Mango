#include <iostream>

struct node {
	int* subStack;
	int subIndex;
	node* next;

	node() {
		subStack = new int[5];
		next = NULL;
		subIndex = 0;
	}

	~node() {
		delete[] subStack;
		next = NULL;
	}
};

class SetOfStacks {
	node* topStack;
	//int topIndex;

public:
	SetOfStacks();
	~SetOfStacks();
	void push(int);
	int pop();
	int popAt(int);
	void show();
};

SetOfStacks::SetOfStacks()
{
	topStack = new node();
	//topIndex = 0;
}

SetOfStacks::~SetOfStacks()
{
	delete topStack;
}

void SetOfStacks::push(int data)
{
	//if (5 == topIndex) {
	if (5 == topStack->subIndex) {
		node* tmp = new node();
		tmp->next = topStack;
		topStack = tmp;
		//topIndex = 0;
	}

	topStack->subStack[topStack->subIndex++] = data;
}

int SetOfStacks::pop()
{
	//if (0 == topIndex) {
	if (0 == topStack->subIndex) {
		if (topStack->next == NULL)
			return -1;

		//topIndex = 5;
		node* tmp = topStack;
		topStack = topStack->next;
		delete tmp;
	}

	return topStack->subStack[--topStack->subIndex];
}

int SetOfStacks::popAt(int n)
{
	node* runner = topStack;
	node* prev = NULL;
	while (--n && runner) {
		prev = runner;
		runner = runner->next;
	}
	if (runner && (0 == runner->subIndex)) {
		if (prev) {
			prev->next = runner->next;
			delete runner;
			if (prev->next)
				return prev->next->subStack[--prev->next->subIndex];
		}

		return -1;
	}
	
	if (NULL == runner)
		return -1;
	return runner->subStack[--runner->subIndex];
}

void SetOfStacks::show()
{
	//for (int i = topIndex - 1; i >= 0; i--)
	//	std::cout << topStack->subStack[i] << " ";
	//node* runner = topStack->next;
	node* runner = topStack;
	while (runner) {
		for (int i = runner->subIndex - 1; i >= 0; i--)
			std::cout << runner->subStack[i] << " ";
		runner = runner->next;
	}

	std::cout << std::endl;
}

int main()
{
	int ch, n;
	SetOfStacks s;
	do {
		std::cout << "1. push\n2. pop\n3. show\n4. exit\n5. pop at\n";
		std::cin >> ch;
		switch (ch) {
			case 1:
				std::cin >> n;
				s.push(n);
				break;
			case 2:
				std::cout << s.pop() << std::endl;
				break;
			case 3:
				s.show();
				break;
			case 5:
				std::cin >> n;
				std::cout << s.popAt(n) << std::endl;
		}
	} while (ch != 4);
	
	return 0;
}
