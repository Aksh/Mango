#include <iostream>
#include <cstdlib>

struct node {
	int data;
	struct node* next;
};

void insert(struct node** head, int data)
{
	if (NULL == *head) {
		*head = new node;
		(*head)->data = data;
		(*head)->next = NULL;
		return;
	}

	node* temp = *head;
	while (temp->next)
		temp = temp->next;
	temp->next = new node;
	temp->next->data = data;
	temp->next->next = NULL;
}

void print(node* first)
{
	while (first) {
		std::cout << first->data << " ";
		first = first->next;
	}

	std::cout << std::endl;
}

int get_nth_to_last(node* head, int n)
{
	node* tmp = head;
	while (tmp && --n)
		tmp = tmp->next;
	
	if (tmp) {
		while (tmp->next) {
			head = head->next;
			tmp = tmp->next;
		}

		return head->data;
	}

	return -1;
}

int main(int argc, char** argv)
{
	node* head = NULL;
	insert(&head, 11);
	insert(&head, 22);
	insert(&head, 33);
	insert(&head, 44);
	insert(&head, 55);
	insert(&head, 66);
	insert(&head, 77);
	insert(&head, 88);
	insert(&head, 99);
	insert(&head, 0);

	print(head);

	std::cout << get_nth_to_last(head, atoi(argv[1])) << std::endl;

	return 0;
}
