#include <iostream>

struct node {
	int data;
	struct node* next;
};

void insert(struct node** head, int data)
{
	if (NULL == *head) {
		*head = new node;
		(*head)->data = data;
		(*head)->next = NULL;
		return;
	}

	node* temp = *head;
	while (temp->next)
		temp = temp->next;
	temp->next = new node;
	temp->next->data = data;
	temp->next->next = NULL;
}

void del(struct node** head, struct node* victim)
{
	if (*head == victim) {
		*head = NULL;
		delete victim;
		return;
	}
	node* temp = *head;
	node* prev;
	while (temp != victim) {
		prev = temp;
		temp = temp->next;
	}
	
	prev->next = temp->next;
	delete temp;
}

void print(node* first)
{
	while (first) {
		std::cout << first->data << " ";
		first = first->next;
	}

	std::cout << std::endl;
}

void remove_dup(node* head)
{
	node* current = head->next;
	node* runner = head;
	node* prev = head;
	node* victim;
	while (current) {
		runner = head;
		while (runner != current) {
			if (runner->data == current->data) {
				prev->next = current->next;
				victim = current;
				current = current->next;
				delete victim;
				break;
			}
			
			runner = runner->next;
		}

		if (runner == current) {
			prev = current;
			current = current->next;
		}
	}
}

int main()
{
	node* head = NULL;
	insert(&head, 77);
	insert(&head, 11);
	insert(&head, 66);
	insert(&head, 22);
	insert(&head, 22);
	insert(&head, 77);
	insert(&head, 33);
	insert(&head, 55);
	insert(&head, 33);
	insert(&head, 44);
	insert(&head, 77);
	insert(&head, 33);
	insert(&head, 22);
	insert(&head, 88);
	insert(&head, 33);
	insert(&head, 33);
	insert(&head, 99);

	print(head);
	remove_dup(head);
	print(head);

	return 0;
}
