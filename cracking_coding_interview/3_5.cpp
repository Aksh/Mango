#include <iostream>
#include <stack>

class Queue {
	std::stack<int> s1;
	std::stack<int> s2;
public:
	Queue() {}
	~Queue() {}
	void enqueue(int data);
	int dequeue();
	//void show();
};

void Queue::enqueue(int data)
{
	s1.push(data);
}

int Queue::dequeue()
{
	if (!s2.empty()) {
		int data = s2.top();
		s2.pop();
		return data;
	}
	
	if (s1.empty()) return -1;
	while (!s1.empty()) {
		s2.push(s1.top());
		s1.pop();
	}

	int data = s2.top();
	s2.pop();
	return data;
}

int main()
{
	int ch, n;
	Queue q;
	do {
		std::cout << "1. enqueue\n2. dequeue\n3. exit\n";
		std::cin >> ch;
		switch (ch) {
			case 1:
				std::cin >> n;
				q.enqueue(n);
				break;
			case 2:
				std::cout << q.dequeue() << std::endl;
				break;
		}
	} while (ch != 3);

	return 0;
}
