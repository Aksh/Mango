#include <iostream>
#include <cstring>

bool isunique(std::string& str)
{
	int all[256] = {0};
	const char* ptr = str.c_str();
	while (*ptr) {
		if (all[*ptr]) return false;
		all[*ptr] = 1;
		ptr++;
	}

	return true;
}

int main()
{
	std::string str;
	std::cin >> str;

	if (isunique(str))
		std::cout << str << " has all unique characters\n";
	else
		std::cout << str << " has not unique characters\n";

	return 0;
}
