#include <iostream>

struct node {
	int data;
	node* left;
	node* right;
};

void insert(node** root, int data)
{
	if (NULL == *root) {
		*root = new node;
		(*root)->left = (*root)->right = NULL;
		(*root)->data = data;
		return;
	}

	if ((*root)->data < data)
		return insert(&((*root)->right), data);
	else
		return insert(&((*root)->left), data);
}

void inorder(node* root)
{
	if (root) {
		inorder(root->left);
		std::cout << root->data << " ";
		inorder(root->right);
	}
}

inline int max(int a, int b)
{
	return a > b ? a : b;
}

inline int min(int a, int b)
{
	return a > b ? b : a;
}

int max_depth(node* root)
{
	if (NULL == root)
		return 0;
	return 1 + max(max_depth(root->left), max_depth(root->right));
}

int min_depth(node* root)
{
	if (NULL == root)
		return 0;
	return 1 + min(min_depth(root->left), min_depth(root->right));
}

void CreateBST(int arr[], int l, int h, node** root)
{
	if (l <= h) {
		int m = (l + h) / 2;
		std::cout << arr[m] << " is inserted\n";
		insert(root, arr[m]);
		CreateBST(arr, m + 1, h, root);
		CreateBST(arr, l, m - 1, root);
	}
}

int main()
{
	int arr[] = {11, 22, 33, 44, 55, 66, 77, 88, 99};
	int l = 0;
	int h = 8;
	node* root = NULL;
	CreateBST(arr, l, h, &root);
	std::cout << "Inorder traversal\n";
	inorder(root);
	std::cout << std::endl;
	std::cout << "Max depth : " << max_depth(root) << std::endl;
	std::cout << "Min depth : " << min_depth(root) << std::endl;

	return 0;
}
