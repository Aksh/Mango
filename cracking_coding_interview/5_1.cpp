#include <iostream>

int main()
{
	int n, m, i, j;
	std::cin >> n >> m >> i >> j;
	int diff = j - i;
	int k = 0;
	int x;
	while (k <= diff) {
		x = m & (1 << k);
		n ^= ((-x ^ n) & (1 << i));
		i++;
		k++;
	}

	std::cout << n << std::endl;

	return 0;
}
