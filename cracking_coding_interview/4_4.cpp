#include <iostream>
#include <vector>
#include <queue>

struct node {
	int data;
	node* left;
	node* right;
};

void insert(node** root, int data)
{
	if (NULL == *root) {
		*root = new node;
		(*root)->left = (*root)->right = NULL;
		(*root)->data = data;
		return;
	}

	if ((*root)->data < data)
		return insert(&((*root)->right), data);
	else
		return insert(&((*root)->left), data);
}

void inorder(node* root)
{
	if (root) {
		inorder(root->left);
		std::cout << root->data << " ";
		inorder(root->right);
	}
}

inline int max(int a, int b)
{
	return a > b ? a : b;
}

inline int min(int a, int b)
{
	return a > b ? b : a;
}

int max_depth(node* root)
{
	if (NULL == root)
		return 0;
	return 1 + max(max_depth(root->left), max_depth(root->right));
}

int min_depth(node* root)
{
	if (NULL == root)
		return 0;
	return 1 + min(min_depth(root->left), min_depth(root->right));
}

void CreateBST(int arr[], int l, int h, node** root)
{
	if (l <= h) {
		int m = (l + h) / 2;
		std::cout << arr[m] << " is inserted\n";
		insert(root, arr[m]);
		CreateBST(arr, m + 1, h, root);
		CreateBST(arr, l, m - 1, root);
	}
}

struct ll {
	node* n;
	ll* next;
};

void insertLL(ll** root, node* n)
{
	if (NULL == *root) {
		*root = new ll;
		(*root)->n = n;
		(*root)->next = NULL;
	}
	else {
		ll* temp = *root;
		while (temp->next)
			temp = temp->next;
		temp->next = new ll;
		temp->next->n = n;
		temp->next->next = NULL;
	}
}

std::vector<ll*> v;

void printLevel(node* root)
{
	std::queue<node*> q;
	q.push(root);
	q.push(NULL);
	node* current = NULL;
	v.push_back(NULL);
	int i = 0;
	while (!q.empty()) {
		current = q.front();
		q.pop();
		if (current) {
			insertLL(&v[i], current);
			if (current->left) q.push(current->left);
			if (current->right) q.push(current->right);
			if (NULL == q.front()) q.push(NULL);
		}
		else {
			v.push_back(NULL);
			i++;
		}
	}
}

void printlist(ll* root)
{
	while (root) {
		std::cout << root->n->data << " ";
		root = root->next;
	}

	std::cout << std::endl;
}

int main()
{
	int arr[] = {11, 22, 33, 44, 55, 66, 77, 88, 99};
	int l = 0;
	int h = 8;
	node* root = NULL;
	CreateBST(arr, l, h, &root);
	std::cout << "Inorder traversal\n";
	inorder(root);
	std::cout << std::endl;
	//std::cout << "Max depth : " << max_depth(root) << std::endl;
	//std::cout << "Min depth : " << min_depth(root) << std::endl;
	printLevel(root);
	std::cout << "Linked list traversal\n";
	for (auto it = v.begin(); it != v.end(); it++)
		printlist(*it);

	return 0;
}
