#include <iostream>

int main()
{
	int a, b;
	std::cin >> a >> b;
	int count = 0;
	for (int c = a ^ b; c != 0; c >>= 1)
		count += c & 1;

	std::cout << count << std::endl;
}
