#include <iostream>
#include <cstdlib>
#include <map>

std::map<unsigned long long, unsigned long long> lookup;
unsigned long long fib(unsigned long long n)
{
	if (!lookup[n]) {
		if (n <= 1)
			lookup[n] = n;
		else
			lookup[n] = fib(n-1) + fib(n - 2);
	}

	return lookup[n];
}

int main(int argc, char** argv)
{
	unsigned long long n;
	n = atoi(argv[1]);
	std::cout << fib(n) << std::endl;
	return 0;
}
