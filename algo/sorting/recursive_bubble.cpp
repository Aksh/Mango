/*
 * Recursive Bubble Sort:
 * Worst and Average Case Time Complexity - O(n^2)
 * Best Case Time Complexity - O(n)
 * Comparison based
 * Stable
 * In-Place
 */

#include <iostream>
#define swap(a, b)  a = a + b; \
					b = a - b; \
					a = a - b;

void rec_bubble(int arr[], int n)
{
	if (1 == n)
		return;
	
	for (int i = 0; i < n - 1; i++) {
		if (arr[i] > arr[i + 1]) {
			swap(arr[i], arr[i + 1]);
		}
	}

	rec_bubble(arr, n - 1);
}

void print(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

int main()
{
	int arr[] = {70, 10, 60, 20, 50, 30, 40};
	std::cout << "before sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));
	rec_bubble(arr, sizeof(arr)/sizeof(arr[0]));
	std::cout << "after sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));

	return 0;
}
