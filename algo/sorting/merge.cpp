/*
 * Insertion Sort:
 * Best, Worst and Average Case Time Complexity - O(nlogn)
 * Comparison based
 * Stable
 * Not In-Place
 */

#include <iostream>

void merge(int arr[], int l, int m, int h)
{
	int n1 = m - l + 1;
	int n2 = h - m;

	int* left = new int[n1];
	int* right = new int[n2];

	for (int i = 0; i < n1; i++)
		left[i] = arr[l + i];
	for (int i = 0; i < n2; i++)
		right[i] = arr[m + 1 + i];

	int i = 0;
	int j = 0;
	int k = l;
	while (i < n1 && j < n2) {
		if (left[i] < right[j])
			arr[k] = left[i++];
		else
			arr[k] = right[j++];
		k++;
	}

	while (i < n1)
		arr[k++] = left[i++];
	
	while (j < n2)
		arr[k++] = right[j++];
}

void merge_sort(int arr[], int l, int h)
{
	if (l < h) {
		int m = (l + h) / 2;
		merge_sort(arr, l, m);
		merge_sort(arr, m + 1, h);
		merge(arr, l, m, h);
	}
}

void print(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

int main()
{
	int arr[] = {70, 10, 60, 20, 50, 30, 40};
	std::cout << "before sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));
	merge_sort(arr, 0, (sizeof(arr)/sizeof(arr[0])) - 1);
	std::cout << "after sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));

	return 0;
}
