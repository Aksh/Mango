/*
 * Insertion Sort:
 * Worst and Average Case Time Complexity - O(n^2)
 * Best Case Time Complexity - O(n)
 * Comparison based
 * Stable
 * In-Place
 */

#include <iostream>

void insertion(int arr[], int n)
{
	for (int i = 1; i < n; i++) {
		int key = arr[i];
		int j = i - 1;

		while (j >= 0 && arr[j] > key) {
			arr[j + 1] = arr[j];
			j--;
		}

		arr[j + 1] = key;
	}
}

void print(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

int main()
{
	int arr[] = {70, 10, 60, 20, 50, 30, 40};
	std::cout << "before sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));
	insertion(arr, sizeof(arr)/sizeof(arr[0]));
	std::cout << "after sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));

	return 0;
}
