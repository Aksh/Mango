/*
 * Bubble Sort:
 * Worst and Average Case Time Complexity - O(n^2)
 * Best Case Time Complexity - O(n)
 * Comparison based
 * Stable
 * In-Place
 */

#include <iostream>

void bubble(int arr[], int n)
{
	for (int i = 0; i < n - 1; i++) {
		int swap = 0;

		// Last i elements are already in place
		for (int j = 0; j < n - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				int t = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = t;
				swap = 1;
			}
		}

		if (0 == swap)
			break;
	}
}

void print(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

int main()
{
	int arr[] = {70, 10, 60, 20, 50, 30, 40};
	std::cout << "before sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));
	bubble(arr, sizeof(arr)/sizeof(arr[0]));
	std::cout << "after sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));

	return 0;
}
