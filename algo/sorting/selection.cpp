/*
 * Selection Sort:
 * Complexity - O(n^2)
 * Comparison based
 * by default not stable, but can be implemented stable one
 * in-Place sort
 */

#include <iostream>

void selection(int arr[], int n)
{
	for (int i = 0; i < n; i++) {
		int min = i;
		for (int j = i + 1; j < n; j++) {
			if (arr[min] > arr[j])
				min = j;
		}

		if (min != i) {
			int t = arr[min];
			arr[min] = arr[i];
			arr[i] = t;
		}
	}
}

void print(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

int main()
{
	int arr[] = {70, 10, 60, 20, 50, 30, 40};
	std::cout << "before sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));
	selection(arr, sizeof(arr)/sizeof(arr[0]));
	std::cout << "after sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));

	return 0;
}
