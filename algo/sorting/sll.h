#ifndef _SLL_H_
#define _SLL_H_

#include <iostream>

struct node {
	int data;
	struct node* next;
};

void insert(node** head, int data);
void printsll(node* first);

#endif
