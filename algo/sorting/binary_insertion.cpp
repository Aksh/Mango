/*
 * Binary Insertion Sort:
 * Worst and Average Case Time Complexity - O(n^2)
 * Best Case Time Complexity - O(n)
 * Comparison based
 * Stable
 * In-Place
 */

#include <iostream>

int getloc(int arr[], int l, int h, int k)
{
	if (l == h) {
		if (arr[l] <= k)
			return l + 1;
		else
			return l;
	}
	int m = (l + h)/2;
	if (arr[m] > k)
		return getloc(arr, l, m, k);
	else
		return getloc(arr, m + 1, h, k);
}

void binsertion(int arr[], int n)
{
	for (int i = 1; i < n; i++) {
		int key = arr[i];
		int j = i - 1;
		int t = getloc(arr, 0, i - 1, key);
		while (j >= t) {
			arr[j + 1] = arr[j];
			j--;
		}

		arr[j + 1] = key;
	}
}

void print(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

int main()
{
	int arr[] = {70, 10, 60, 20, 50, 30, 40};
	std::cout << "before sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));
	binsertion(arr, sizeof(arr)/sizeof(arr[0]));
	std::cout << "after sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));

	return 0;
}
