#include "sll.h"

void insert(node** head, int data)
{
	node* n = new node();
	n->data = data;
	n->next = *head;
	*head = n;
}

void printsll(node* first)
{
	while (first) {
		std::cout << first->data << " ";
		first = first->next;
	}

	std::cout << std::endl;
}
