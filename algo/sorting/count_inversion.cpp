/*
 * Count inversion
 * Time Complexity - O(n^2)
 */

#include <iostream>

int inversions(int arr[], int n)
{
	int count = 0;
	for (int i = 0; i < n - 1; i++) {
		int j = i + 1;
		while (j < n) {
			if (arr[i] > arr[j]) {
				std::cout << "(" << arr[i] << ", " << arr[j] << ") ";
				count++;
			}
			j++;
		}
	}

	std::cout << std::endl;

	return count;
}

int main()
{
	int arr[] = {2, 4, 1, 3, 5};
	int count = inversions(arr, 5);
	std::cout << "Inversion Count : " << count << std::endl;

	return 0;
}
