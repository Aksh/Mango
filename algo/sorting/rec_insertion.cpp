/*
 * Recursive Insertion Sort:
 * Worst and Average Case Time Complexity - O(n^2)
 * Best Case Time Complexity - O(n)
 * Comparison based
 * Stable
 * In-Place
 */

#include <iostream>

void rinsertion(int arr[], int n)
{
	if (n <= 1)
		return;
	
	rinsertion(arr, n - 1);
	int last = arr[n - 1];
	int j = n - 2;
	while (j >= 0 && arr[j] > last) {
		arr[j + 1] = arr[j];
		j--;
	}

	arr[j + 1] = last;
}

void print(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

int main()
{
	int arr[] = {70, 10, 60, 20, 50, 30, 40};
	std::cout << "before sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));
	rinsertion(arr, sizeof(arr)/sizeof(arr[0]));
	std::cout << "after sorting\n";
	print(arr, sizeof(arr)/sizeof(arr[0]));

	return 0;
}
