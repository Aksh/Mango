#include <iostream>
#include <queue>
#include <list>

using namespace std;

class Graph {
	int V;
	list<int> *adj;

public:
	Graph(int V);
	void addEdge(int src, int dst);
	void bfs(int s);
	void printGraph();
};

Graph::Graph(int V)
{
	this->V = V;
	this->adj = new list<int>[V];
}

void Graph::addEdge(int src, int dst)
{
	adj[src].push_back(dst);
}

void Graph::bfs(int s)
{
	bool *visited = new bool[V];
	for (int i = 0; i < V; i++)
		visited[i] = false;
	
	list<int> q;
	q.push_back(s);
	visited[s] = true;

	list<int>::iterator it;
	while (!q.empty()) {
		s = q.front();
		q.pop_front();

		cout << s << " ";
		for (it = adj[s].begin();
			it != adj[s].end();
			it++) {
			if (!visited[*it]) {
				q.push_back(*it);
				visited[*it] = true;
			}
		}
	}

	cout << endl;
}

void Graph::printGraph()
{
	for (int i = 0; i < V; i++) {
		cout << i << " => ";
		for (list<int>::iterator it = adj[i].begin();
			it != adj[i].end();
			it++)
			cout << *it << " => ";
		cout << "NULL" << endl;
	}
}

int main()
{
	//Graph g(4);
	//g.addEdge(0, 1);
	//g.addEdge(0, 2);
	//g.addEdge(1, 2);
	//g.addEdge(2, 0);
	//g.addEdge(2, 3);
	//g.addEdge(3, 3);
	
	Graph g(7);
	g.addEdge(0, 1);
	g.addEdge(1, 0);
	g.addEdge(0, 2);
	g.addEdge(2, 0);
	g.addEdge(0, 3);
	g.addEdge(3, 0);
	g.addEdge(0, 4);
	g.addEdge(4, 0);
	g.addEdge(3, 6);
	g.addEdge(6, 3);
	g.addEdge(4, 5);
	g.addEdge(5, 4);
	g.addEdge(4, 6);
	g.addEdge(6, 4);
	g.addEdge(5, 6);
	g.addEdge(6, 5);

	//g.printGraph();

	cout << "========= BFS ===========\n";
	g.bfs(0);

	return 0;
}
