#!/bash/bin/perl
use warnings;
use Scalar::Util qw(looks_like_number);

my $n = <STDIN>;
my %weight;
while ($n--) {
	my $line = <STDIN>;
	chop($line);
	my $who = substr $line, 0, 1;
	@tokens = split(/ /, $line);
	foreach my $token (@tokens) {
		if (looks_like_number($token)) {
			if ($who eq 'G') {
				$weight{$token} += 2;
			}
			else {
				$weight{$token} += 1;
			}
		}
	}
}

my $max1 = 0;
my $max2 = 0;
my $date = 0;

foreach my $key (keys %weight) {
	if ($weight{$key} > $max1) {
		$max1 = $weight{$key};
		$date = $key;
	}
	elsif ($weight{$key} == $max1) {
		$max2 = $weight{$key};
	}
}

if ($max1 != $max2) {
	if ($date == 20 || $date == 19) {
		print "Date";
	}
	else {
		print "No Date";
	}
}
else {
	print "No Date";
}
