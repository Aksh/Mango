#include <stdio.h>
#include <string.h>

int main()
{
	int n;
	char* line;
	int len;
	int suvo = 0;
	int suvojit = 0;

	scanf("%d", &n);
	getchar();
	while (n--) {
		line = (char *)malloc(151);
		fgets (line, 150, stdin);
		while (*line) {
			if (*line == 'S') {
				if (0 == strncmp("SUVOJIT", line, 7))
					suvojit++;
				else
				if (0 == strncmp("SUVO", line, 4))
					suvo++;
				line++;
			}
			else
				line++;
		}

		printf("SUVO = %d, SUVOJIT = %d\n", suvo, suvojit);
		suvo = 0;
		suvojit = 0;
	}

}
