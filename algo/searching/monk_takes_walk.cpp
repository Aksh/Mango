#include <iostream>

int main()
{
    int n;
    std::cin >> n;
    std::string s;
    while (n--) {
        s.clear();
        std::cin >> s;
        int len = s.length();
        int count = 0;
        for (int i = 0; i < len; i++) {
            if (s[i] == 'a' || s[i] == 'A' || s[i] == 'e' || s[i] == 'E' || s[i] == 'i' || s[i] == 'I' || s[i] == 'o' || s[i] == 'O' || s[i] == 'u' || s[i] == 'U')
                count++;
        }
        
        std::cout << count << std::endl;
    }
    
    return 0;
}
