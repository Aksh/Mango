#!/bash/bin/perl
use warnings;
use strict;

my %list;
my $n = <STDIN>;
print $n;
for (my $i = 0; $i < $n; $i++) {
	print "$i - $n";
	$list{$i} = <STDIN>;
	chop($list{$i});
}

my $k = <STDIN>;
my %by_value = reverse %list;
print $by_value{$k};
