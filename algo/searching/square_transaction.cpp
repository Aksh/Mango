#include <iostream>

int main()
{
	int t;
	std::cin >> t;

	int a[10000];
	for (int i = 0; i < t; i++)
		std::cin >> a[i];
	
	int q;
	std::cin >> q;
	while (q--) {
		int target;
		std::cin >> target;
		int total = 0;
		int ach = 0;
		for (int i = 0; i < t; i++) {
			total += a[i];
			if (total >= target) {
				std::cout << i + 1 << std::endl;
				ach = 1;
				break;
			}
		}

		if (!ach)
			std::cout << "-1" << std::endl;
	}

	return 0;
}
