//
// Open Close Principle
// Software entities like classes, modules and functions should be open for extension but closed for modifications.
// For more details visit : http://www.oodesign.com/design-principles.html
//
#include <iostream>

class city {
	public:
			virtual long population() = 0;
};

class pune : public city {
	public:
			long population() {
				return 250000;
			}
};

class mumbai : public city {
	public:
			long population() {
				return 1000000;
			}
};

class calculate {
	public:
			void show_pop(city* c) {
				std::cout << c->population() << std::endl;
			}
};

int main() 
{
	calculate c;
	c.show_pop(new pune);
	c.show_pop(new mumbai);
}
