//
// Single Resposibility Principle
// A class should have only one reason to change
// For more details visit : http://www.oodesign.com/single-responsibility-principle.html
//

#include <iostream>

class IContent {
	public:
		virtual std::string getAsString() = 0;
};

class html : public IContent {
	std::string c;
	public:
		html(std::string c) {
			this->c = c;
		}

		std::string getAsString() {
			// return html content in string format;
			std::cout << "html string\n";
			return c;
		}
};

class IEmail {
	public:
		virtual void setSender(std::string) = 0;
		virtual void setReceiver(std::string) = 0;
		virtual void setContent(IContent*) = 0;
};

class pop3 : public IEmail {
	std::string sender;
	std::string receiver;
	public:
		void setSender(std::string s) {
			// set sender according to pop3
			std::cout << "pop3::setSender\n";
			sender = s;
		}

		void setReceiver(std::string r) {
			// set receiver according to pop3
			std::cout << "pop3::setReceiver\n";
			receiver = r;
		}

		void setContent(IContent* content) {
			// set content according to pop3
			std::cout << "pop3::setContent\n";
			content->getAsString();
		}
};

int main()
{
	pop3 e;
	e.setSender("akshaynikam49@gmail.com");
	e.setReceiver("aksh_92@yahoo.in");
	html h("Hii, this is single responsibility principle");
	e.setContent(&h);
	return 0;
}
