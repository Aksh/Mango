// 
// Interface Segregation Principle
// Clients should not be forced to depend upon interfaces that they don't use.
// For more details visit : http://www.oodesign.com/interface-segregation-principle.html
//

#include <iostream>

class iworkable {
	public:
			virtual void work() = 0;
};

class ieatable {
	public:
			virtual void eat() = 0;
};

class worker : public iworkable, public ieatable {
	public:
			void work() {
				std::cout << "Worker: Working\n";
			}

			void eat() {
				std::cout << "Worker: Eating in lunch break\n";
			}
};

class robot : public iworkable {
	public:
			void work() {
				std::cout << "Robot: Working\n";
			}
};

class superWorker : public iworkable, public ieatable {
	public:
			void work() {
				std::cout << "Super Worker: Working hard\n";
			}

			void eat() {
				std::cout << "Super Worer: Eating in lunch break\n";
			}
};

class manager {
	public:
			void manage(iworkable* w) {
				w->work();
			}
};

int main() {
	manager m;
	m.manage(new worker);
	m.manage(new robot);
	m.manage(new superWorker);
	return 0;
}
