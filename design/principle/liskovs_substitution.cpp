//
// Liskov's Substitution Principle
// Derived types must be completely substitutable for their base types
// For more details visit : http://www.oodesign.com/liskov-s-substitution-principle.html
//

#include <iostream>

class rectangle {
	protected:
		double m_width;
		double m_height;

	public:
		virtual void setWidth(double w) {
			m_width = w;
		}

		virtual void setHeight(double h) {
			m_height = h;
		}

		double getWidth() {
			return m_width;
		}

		double getHeight() {
			return m_height;
		}

		double area() {
			return m_width * m_height;
		}
};

class square : public rectangle {
	public:
		void setWidth(double w) {
			m_width = w;
			m_height = w;
		}

		void setHeight(double h) {
			m_width = h;
			m_height = h;
		}
};

rectangle*
getNewObject(int i)
{
	switch (i) {
		case 1:
			return new rectangle;
		case 2:
			return new square;
	}
}

int 
main()
{
	rectangle* r;
	std::cout << "===========================\n";
	std::cout << "Get Rectangle Object\n";
	r = getNewObject(1);
	std::cout << "Set Width 5\n";
	r->setWidth(5);
	std::cout << "Set Height 10\n";
	r->setHeight(10);
	std::cout << "Area of rectangle : " << r->area() << std::endl;
	std::cout << "===========================\n";
	std::cout << "===========================\n";
	delete r;
	std::cout << "Get Square Object\n";
	r = getNewObject(2);
	std::cout << "Set Width 5\n";
	r->setWidth(5);
	std::cout << "Set Height 10\n";
	r->setHeight(10);
	std::cout << "Area of square : " << r->area() << std::endl;
	std::cout << "===========================\n";
	delete r;

	return 0;
}
