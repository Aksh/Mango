//
// Dependency Inversion Principle
// High-level modules should not depend on low-level modules. Both should depend on abstractions.
// Abstractions should not depend on details. Details should depend on abstractions.
// For more details visit : http://www.oodesign.com/dependency-inversion-principle.html
//
#include <iostream>

class employee {
	public:
			virtual void work() = 0;
};

class aksh : public employee {
	public:
			void work() {
				std::cout << "Aksh: working\n";
			}
};

class sushil : public employee {
	public:
			void work() {
				std::cout << "Sushil Sir: working hard\n";
			}
};

class amol : public employee {
	public:
			void work() {
				std::cout << "Amol Sir: Working soooooo hard from the bottom of his heart\n";
			}
};

class manager {
	public:
			void manage(employee* e) {
				e->work();
			}
};

int main()
{
	manager nilesh;
	nilesh.manage(new aksh);
	nilesh.manage(new sushil);
	nilesh.manage(new amol);
	return 0;
}
