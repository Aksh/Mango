#include "shape.h"
#include "fct.h"
#include <stdlib.h>

int main(int argc, char** argv)
{
	if (2 != argc) {
		std::cout << "usage: <bin> <shape>\n";
		exit(EXIT_FAILURE);
	}
	shape* obj = GetShape(argv[1]);
	if (!obj) {
		std::cout << "Invalid argument\n";
		exit(EXIT_FAILURE);
	}
	obj->who();
	delete obj;
	return 0;
}
