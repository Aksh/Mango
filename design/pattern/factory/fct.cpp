#include "fct.h"
#include "shape.h"
#include "circle.h"
#include "square.h"

shape*
GetShape(std::string str)
{
	if ("circle" == str)
		return new circle;
	else
	if ("square" == str)
		return new square;
	else
		return NULL;
}
