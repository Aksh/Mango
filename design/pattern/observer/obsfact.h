#ifndef _H_OBSFACT_H__
#define _H_OBSFACT_H__

#include "observer.h"
#include <iostream>

observer*
GetObserver(std::string);

#endif
