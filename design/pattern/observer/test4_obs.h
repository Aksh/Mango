#ifndef _H_TEST_4_OBSERVER_H__
#define _H_TEST_4_OBSERVER_H__

#include "observer.h"
#include <iostream>

class test4 : public observer {
	public:
		void update();
};

#endif
