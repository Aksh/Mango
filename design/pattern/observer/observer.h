#ifndef _H_OBSERVER_H__
#define _H_OBSERVER_H__

class observer {
	public:
			virtual void update() = 0;
};

#endif
