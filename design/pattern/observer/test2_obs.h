#ifndef _H_TEST_2_OBSERVER_H__
#define _H_TEST_2_OBSERVER_H__

#include "observer.h"
#include <iostream>

class test2 : public observer {
	public:
		void update();
};

#endif
