#ifndef _H_TEST_1_OBSERVER_H__
#define _H_TEST_1_OBSERVER_H__

#include "observer.h"
#include <iostream>

class test1 : public observer {
	public:
		void update();
};

#endif
