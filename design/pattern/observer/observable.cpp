#include "observable.h"

observable::observable()
{
	state = 0;
}

observable::~observable()
{
	;
}

void
observable::attach(observer* obj)
{
	observerList.push_back(obj);
}

void
observable::detach(observer* obj)
{
	if (observerList.end() != std::find(observerList.begin(), observerList.end(), obj)) {
		observerList.remove(obj);
	}
}

void
observable::notify()
{
	std::list <observer*>::iterator it = observerList.begin();
	while (it != observerList.end()) {
		(*it)->update();
		it++;
	}
}

int
observable::getState()
{
	return state;
}
