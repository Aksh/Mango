#include "obsfact.h"
#include "test1_obs.h"
#include "test2_obs.h"
#include "test3_obs.h"
#include "test4_obs.h"

observer*
GetObserver(std::string obs)
{
	if ("test1" == obs) {
		return new test1;
	}
	else
	if ("test2" == obs) {
		return new test2;
	}
	else
	if ("test3" == obs) {
		return new test3;
	}
	else
	if ("test4" == obs) {
		return new test4;
	}

	return NULL;
}
