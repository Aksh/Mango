#include "observable.h"
#include "obscmn.h"
#include "obsfact.h"
#include <iostream>

int main()
{
	observable obj;
	std::string* str = GetModulesList();
	while ("Last" != *str) {
		observer* o = GetObserver(*str);
		if (o) {
			std::cout << "obj.attach(&" << *str << ")\n";
			obj.attach(o);
		}
		str++;
	}
	std::cout << "obj.notify\n";
	obj.notify();
	return 0;
}
