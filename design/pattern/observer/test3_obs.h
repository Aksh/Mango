#ifndef _H_TEST_3_OBSERVER_H__
#define _H_TEST_3_OBSERVER_H__

#include "observer.h"
#include <iostream>

class test3 : public observer {
	public:
		void update();
};

#endif
