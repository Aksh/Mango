#ifndef _H_OBSERVABLE_H__
#define _H_OBSERVABLE_H__
#include "observer.h"
#include <iostream>
#include <list>
#include <algorithm>

class observable {
	private:
			int state;
			std::list <observer*> observerList;
	public:
			observable();
			~observable();
			void attach(observer*);
			void detach(observer*);
			int getState();
			void notify();
};

#endif
