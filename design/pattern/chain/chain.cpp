#include <iostream>

class handler {
	handler* next;
	public:
		handler() : next(NULL) {}
		void set_successor(handler* obj) {
			next = obj;
		}

		virtual void handle_request(int i) {
			if (next)
				next->handle_request(i);
			else {
				std::cout << "Request is handled by all modules\n";
			}
		}
};

class handler1 : public handler {
	public:
		void handle_request(int i) {
			std::cout << "Handling request in 1 : " << i << std::endl;
			handler::handle_request(i);
		}
};

class handler2 : public handler {
	public:
		void handle_request(int i) {
			std::cout << "Handling request in 2 : " << i << std::endl;
			handler::handle_request(i);
		}
};

class handler3 : public handler {
	public:
		void handle_request(int i) {
			std::cout << "Handling request in 3 : " << i << std::endl;
			handler::handle_request(i);
		}
};

int main() {
	handler1 h1;
	handler2 h2;
	handler3 h3;

	h1.set_successor(&h2);
	h2.set_successor(&h3);

	h1.handle_request(22);

	return 0;
}
