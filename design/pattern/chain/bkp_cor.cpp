#include <iostream>

class handler {
	handler* next;
	public:
		handler() : next(NULL) {}
		void set_successor(handler* obj) {
			next = obj;
		}

		virtual void handle_request(int i) {
			if (next)
				next->handle_request(i);
			else {
				std::cout << "Request is handled by all modules\n";
			}
		}
};

class _2_1 : public handler {
	public:
		void handle_request(int i) {
			std::cout << "Handling request in 1 : " << i << std::endl;
			handler::handle_request(i);
		}
};

class _2_2 : public handler {
	public:
		void handle_request(int i) {
			std::cout << "Handling request in 2 : " << i << std::endl;
			handler::handle_request(i);
		}
};

class _2_3 : public handler {
	public:
		void handle_request(int i) {
			std::cout << "Handling request in 3 : " << i << std::endl;
			handler::handle_request(i);
		}
};

int main() {
	_2_1 h1;	// 2.1
	_2_2 h2;	// 2.2
	_2_3 h3;	// 2.3

	h1.set_successor(&h2);
	h2.set_successor(&h3);

	h1.handle_request(22);

	return 0;
}
