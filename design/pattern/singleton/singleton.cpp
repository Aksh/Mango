//
// Implemented a singleton class
//

#include "singleton.h"

//
// global variables
//
bool CDateTime::m_Initialized = false;
CDateTime* CDateTime::m_pCDateTime = NULL;

//
// Member function definitions
//
CDateTime::CDateTime()
{
	char ch;
	std::cout << "Current Time Updated\n\n";
	m_tCurrentTime = time(NULL);
	m_Initialized = true;
	struct tm *ConvertedTime;
	char szBuffer[256] = {0};
	ConvertedTime = localtime(&m_tCurrentTime);
	m_strDTFormat = "%A, %b, %d, %Y. The Local Time is: %X";
	strftime (szBuffer, sizeof(szBuffer), m_strDTFormat.c_str(), ConvertedTime);
	m_strTime.assign(szBuffer);
}

CDateTime*
CDateTime::GetInstance()
{
	if (m_Initialized == false) {
		m_pCDateTime = new CDateTime();
		return m_pCDateTime;
	}
	return m_pCDateTime;
}

void
CDateTime::ReleaseInstance()
{
	if (m_pCDateTime) {
		delete m_pCDateTime;
		m_pCDateTime = NULL;
	}
}

void
CDateTime::ShowTime()
{
	std::cout << m_strTime << std::endl;
}

CDateTime::~CDateTime()
{
	m_Initialized = false;
}
