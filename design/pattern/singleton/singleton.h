#include <iostream>
#include <time.h>

class CDateTime {
	public:
		static CDateTime*
		GetInstance();

		void
		ReleaseInstance();

		void
		ShowTime();

	private:
		CDateTime();

		~CDateTime();

		static bool m_Initialized;
		static CDateTime* m_pCDateTime;
		time_t  m_tCurrentTime;
		std::string m_strTime;
		std::string m_strDTFormat;
};
