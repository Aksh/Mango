#include <iostream>
#include "singleton.h"

int main()
{
	#if 1
	CDateTime* pCDateTime = NULL;
	int ch;
	do {
		std::cout << "1. GetInstance\n2. ShowTime\n3. ReleaseInstance\n4.Exit\nEnter : ";
		std::cin >> ch;
		switch (ch) {
			case 1:
				pCDateTime = CDateTime::GetInstance();
				break;
			case 2:
				if (pCDateTime != NULL)
					pCDateTime->ShowTime();
				break;
			case 3:
				pCDateTime->ReleaseInstance();
				break;
		}	
	} while (ch != 4);
	#endif
	return 0;
}
