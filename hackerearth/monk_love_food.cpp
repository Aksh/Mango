#include <iostream>
#include <stack>

int main()
{
	std::stack<int> food;
	int n;
	std::cin >> n;
	int c;
	int q;
	while (n) {
		std::cin >> q;
		switch (q) {
			case 1:
				if (0 == food.size()) std::cout << "No Food\n";
				else {
					std::cout << food.top() << std::endl;
					food.pop();
				}
				break;
			case 2:
				std::cin >> c;
				food.push(c);
				break;
		}

		n--;
	}

	return 0;
}
