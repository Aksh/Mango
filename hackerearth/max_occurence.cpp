#include <iostream>
#include <cstring>

int hash(char c)
{
	return c;
}

int main()
{
	int arr[128];
	memset(arr, 0, sizeof(arr));
	std::string str;
	std::getline(std::cin, str);

	int index = 0;
	int i = 0;
	int len = str.length();
	while (i < len) {
		arr[str.at(i)]++;
		i++;
	}

	int max = 0;
	for (int i = 0; i < 128; i++) {
		if (max < arr[i]) {
			max = arr[i];
			index = i;
		}
	}

	std::cout.put(index);
	std::cout << " " << max << std::endl;

	return 0;
}
