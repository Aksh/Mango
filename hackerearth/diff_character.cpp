#include <iostream>
#include <algorithm>
#include <map>
#include <vector>

bool map_sort(std::pair<char, int> t1, std::pair<char, int> t2)
{
	return t1.second > t2.second;
}

int main()
{
	std::map<char, int> hash;
	for (char c = 'z'; c >= 'a'; c--)
		hash[c] = 0;

	int t;
	std::string str;
	std::cin >> t;

	while (t--) {
		str.clear();
		std::cin >> str;
		int i = 0;
		int len = str.length();
		while (i < len) {
			hash[str.at(i)]++;
			i++;
		}

		std::vector<std::pair<char, int> > mapV;
		for (std::map<char, int>::iterator it = hash.begin();
			it != hash.end();
			it++)
			mapV.push_back(*it);
		std::stable_sort(mapV.begin(), mapV.end(), map_sort);
		for (std::vector<std::pair<char, int> >::reverse_iterator it = mapV.rbegin();
			it != mapV.rend();
			it++)
			std::cout << (*it).first << " ";
		std::cout << std::endl;
	}
	
	return 0;
}
