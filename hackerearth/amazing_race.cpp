#include <iostream>

int main()
{
	int a[100000] = {0};
	int n;
	int t;
	std::cin >> t;
	while (t--) {
		std::cin >> n;
		for (int i = 0; i < n; i++) std::cin >> a[i];
		int sight = 0;
		int max = 0;
		int pos = 0;
		for (int i = 0; i < n; i++) {
			int x = 0;
			int j = i - 1;
			while (j >= 0) {
				if (a[j] < a[i]) x++;
				else {
					x++;
					break;
				}

				j--;
			}

			j = i + 1;
			//std::cout << "j : " << j << " i : " << i << std::endl;
			while (j < n) {
				//std::cout << "a[j] : " << a[j] << " a[i] : " << a[i] << std::endl;
				if (a[j] < a[i]) x++;
				else {
					x++;
					break;
				}

				j++;
			}

			sight = x * (i + 1);
			if (max < sight) {
				max = sight;
				pos = i;
			}

			//std::cout << "x : " << x << " sight : " << sight << std::endl;
		}

		std::cout << pos + 1 << std::endl;
	}
	return 0;
}
