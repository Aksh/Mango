#include <iostream>
#include <utility>
#include <queue>

int main()
{
	std::pair<int, int> slice;
	std::queue<std::pair<int, int> > p, q;
	int n, x, pw;
	std::cin >> n >> x;

	for (int i = 0; i < n; i++) {
		std::cin >> pw;
		slice = std::make_pair(pw, i + 1);
		q.push(slice);
	}

	for (int i = 0; i < x; i++) {
		//std::cout << i << std::endl;
		int j = 0;
		int max = 0;
		int index = -1;
		while (!q.empty() && (j++ < x)) {
			if (max < q.front().first) {
				max = q.front().first;
				index = q.front().second;
			}

			p.push(q.front());
			q.pop();
		}

		if (-1 == index) index = p.front().second;
		std::cout << index << " ";
		while (!p.empty()) {
			if (index != p.front().second) {
				if (p.front().first)
					p.front().first--;
				q.push(p.front());
			}
			
			p.pop();
		}
	}

	return 0;
}
