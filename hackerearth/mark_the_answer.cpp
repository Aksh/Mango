#include <iostream>

int main()
{
	int a[100000] = {0};
	int n;
	std::cin >> n;
	int x;
	std::cin >> x;
	for (int i = 0; i < n; i++) std::cin >> a[i];
	bool skip = false;
	int marks = 0;
	for (int i = 0; i < n; i++) {
		if (x >= a[i]) marks++;
		else {
			if (skip) break;
			skip = true;
		}
	}

	std::cout << marks << std::endl;
	return 0;
}
