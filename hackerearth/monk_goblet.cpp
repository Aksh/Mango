#include <iostream>
#include <queue>

struct node {
	int school;
	std::queue<int> q;
	node* next;
};

node* head = NULL;

void enque(int x, int y)
{
	node* temp = head;
	node* prev = NULL;
	while (temp && temp->school != x) {
		prev = temp;
		temp = temp->next;
	}

	if (!temp) {
		temp = new node();
		temp->school = x;
		temp->next = NULL;
		if (prev) prev->next = temp;
	}

	temp->q.push(y);
	if (!head) head = temp;
}

void deque(int& x, int& y)
{
	node* temp = head;
	x = temp->school;
	y = temp->q.front();
	temp->q.pop();
	if (temp->q.empty()) {
		head = head->next;
		delete temp;
	}
}

int main()
{
	int q, x, y;
	std::cin >> q;

	while (q--) {
		char op;
		std::cin >> op;
		switch (op) {
			case 'E':
				std::cin >> x >> y;
				enque(x, y);
				//print();
				break;

			case 'D':
				deque(x, y);
				//print();
				std::cout << x << " " << y << std::endl;
				break;
		}
	}
	return 0;
}
