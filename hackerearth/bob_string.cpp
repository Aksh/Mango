#include <iostream>
#include <string>

void check_anagram(std::string& s, std::string& t)
{
	int hashS[26] = {0};
	int hashT[26] = {0};

	int i = 0;
	int len = s.length();
	while (i < len) {
		hashS[s.at(i) - 'a']++;
		i++;
	}

	i = 0;
	len = t.length();
	while (i < len) {
		hashT[t.at(i) - 'a']++;
		i++;
	}

	int count = 0;
	for (int i = 0; i < 26; i++) {
		if (hashS[i] != hashT[i])
			count++;
	}

	std::cout << count << std::endl;
}

int main()
{
	int q;
	std::cin >> q;
	std::string s;
	std::string t;

	while (q--) {
		s.clear();
		t.clear();
		std::cin >> s >> t;
		check_anagram(s, t);
	}

	return 0;
}
