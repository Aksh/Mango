#include <iostream>
#include <cstring>

int main()
{
	int hash[26] = {0};
	int q;
	std::cin >> q;
	while (q--) {
		memset(hash, 0, sizeof(hash));
		std::string str;
		std::cin >> str;
		int i = 0;
		int len = str.length();
		while (i < len) {
			hash[str.at(i) - 'a']++;
			i++;
		}
		
		int count = 0;
		for (i = 0; i < 26; i++) {
			if (hash[i] % 2)
				count++;
		}

		if (1 == count || 0 == count)
			std::cout << 0 << std::endl;
		else
			std::cout << count - 1 << std::endl;
	}

	return 0;
}
