#include <iostream>
#include <stack>

int main()
{
	int a[5000] = {0};
	int n;
	std::cin >> n;
	std::stack<int> s;
	for (int i = 0; i < n; i++) std::cin >> a[i];
	int prev = 0;
	int sum = 0;
	for (int i = 0; i < n; i++) {
		if (a[i]) {
			sum += a[i];
			s.push(a[i]);
		}
		else {
			if (s.size()) {
				sum -= s.top();
				s.pop();
			}
		}
	}

	std::cout << sum << std::endl;
	return 0;
}
