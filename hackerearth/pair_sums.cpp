#include <iostream>

void pair_sum(int* a, int n, int x)
{
	int binHash[1000000] = {0};
	for (int i = 0; i < n; i++) {
		if ((x - a[i]) && (binHash[x - a[i]])) {
			std::cout << "YES" << std::endl;
			return;
		}
		binHash[a[i]] = 1;
	}

	std::cout << "NO" << std::endl;
}

int main()
{
	int n, x;
	std::cin >> n >> x;
	int* arr = new int[n];
	for (int i = 0; i < n; i++)
		std::cin >> arr[i];
	pair_sum(arr, n, x);
	return 0;
}
