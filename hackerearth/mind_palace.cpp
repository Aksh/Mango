#include <iostream>

void search_in_mind_palace(int** a, int n, int m, int x)
{
	int i = 0;
	int j = 0;
	while (i >= 0 && i < n && j >= 0 && j < m) {
		std::cout << a[i][j] << " : " << i << " : " << j << std::endl;
		if (x > a[i][j]) { 
			if (j + 1 == m) i++;
			else j++;
		}
		else
		if (x < a[i][j]) {
			while (j >= 0 && x < a[i][j]) j--;
			if (j >= 0 && x == a[i][j]) {
				std::cout << i << " " << j << std::endl;
				return;
			}
			else i++;
		}
		else {
			std::cout << i << " " << j << std::endl;
			return;
		}
	}

	std::cout << "-1 -1" << std::endl;
}

int main()
{
	int n, m;
	std::cin >> n >> m;
	int** arr = new int*[n];
	for (int i = 0; i < n; i++) {
		arr[i] = new int[m];
		for (int j = 0; j < m; j++)
			std::cin >> arr[i][j];
	}

	int q;
	std::cin >> q;
	int x;
	while (q--) {
		std::cin >> x;
		search_in_mind_palace(arr, n, m, x);
	}

	return 0;
}
