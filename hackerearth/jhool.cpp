#include <iostream>
#include <queue>

struct node {
	int data;
	node* child;
	node* sibling;
	node* parent;

	node(int data) {
		this->data = data;
		child = NULL;
		sibling = NULL;
		parent = NULL;
	}
};

std::queue<node*> q;

void addchild(node* cur, int data)
{
	node* sib = cur->child;
	//std::clog << "addchild => cur->child : " << cur->child << std::endl;
	if (cur->child) {
		while (sib->sibling) {
			if (sib->data == data) return;
			sib = sib->sibling;
		}
		
		if (data == sib->data) return;
		sib->sibling = new node(data);
		sib->sibling->parent = sib->parent;
	}
	else {
		//std::clog << "cur : " << cur->data << std::endl;
		cur->child = new node(data);
		cur->child->parent = cur;
	}
}

void insert(node** sol, int data, int level)
{
	if (NULL == *sol) return;
	node* cur = *sol;
	while (level--) cur = cur->child;
	node* temp = cur->parent;
	do {
		//std::clog << "current level for : " << cur->data << "\n";
		while (cur) {
			//std::clog << "addchild(cur, " << data << ");\n";
			addchild(cur, data);
			//std::clog << "addchild returns\n";
			//std::clog << "node added for child of : " << cur->data << std::endl;
			cur = cur->sibling;
		}
			
		if (temp) temp = temp->sibling;
		if (temp) cur = temp->child;
	} while (temp);
	//node* sib = cur->child;
	//if (cur->child) {
	//	while (sib->sibling) {
	//		if (data == sib->data) return;
	//		sib = sib->sibling;
	//	}
	//	if (data == sib->data) return;
	//	sib->sibling = new node(data);
	//}
	//else {
	//	node* temp = cur;
	//	while (temp) {
	//		temp->child = new node(data);
	//		temp = temp->sibling;
	//	}
	//}
}

void getchild(node* sol, std::queue<node*>& temp)
{
	if (0 == sol) return;
	while (sol) {
		temp.push(sol->child);
		sol = sol->sibling;
	}
}

void getlevelchild(node* sol, std::queue<node*>& temp, int level)
{
	level = level + 1;
	while (level--) {
		getchild(sol, temp);
		if (0 == temp.size()) break;
		sol = temp.front();
		temp.pop();
	}
}

void print_level(node* sol, int level)
{
	std::queue<node*> temp;
	getlevelchild(sol, temp, level);
	while (temp.size()) {
		node* t = temp.front();
		while (t) {
			std::cout << t->data << " ";
			t = t->sibling;
		}
		temp.pop();
	}

	std::cout << std::endl;
}

void print_sib(node* sib)
{
	while (sib) {
		if (sib->child) {
			q.push(sib->child);
			std::clog << "push(" << sib->child->data << ");\n";
		}
		std::cout << sib->data << " ";
		sib = sib->sibling;
	}
	//q.push((node*) -1);
	//std::cout << std::endl;
}

void print(node* sol)
{
	//print_level(sol, 2);
	//return;
	q.empty();
	q.push(sol);
	//std::cout << "push(" << sol->data << ");\n";
	while (q.size()) {
		node* e = q.front();
		//if (e == (node *)-1) std::cout << std::endl;
		//else print_sib(e);
		print_sib(e);
		q.pop();
	}
	std::clog << std::endl;
	//print_sib(sol);//-1
	//sol = sol->child;
	//print_sib(sol);//0 1
	//sol = sol->child;
	//print_sib(sol);//1 2
	//sol = sol->parent->sibling;
	//if (sol)
	//	sol = sol->child;
	//print_sib(sol);//1 2
	//sol = sol->parent->parent->child->child->child;
	//print_sib(sol);//2 0
	//sol = sol->parent->sibling->child;
	//print_sib(sol);//2 0
	//sol = sol->parent->parent->sibling->child->child;
	//print_sib(sol);//2 0
	//sol = sol->parent->sibling->child;
	//print_sib(sol);//2 0

	//while (sol) {
	//	std::cout << sol->data << " ";
	//	node* sib = sol->sibling;
	//	while (sib) {
	//		std::cout << sib->data << " ";
	//		sib = sib->sibling;
	//	}
	//	std::cout << std::endl;
	//	sol = sol->child;
	//}
}

int main()
{
	int n, m;
	std::cin >> n >> m;
	node* sol = new node(-1);
	for (int i = 0; i < m; i++) {
		int x, y;
		std::cin >> x >> y;
		for (int j = x; j != y;) {
			//std::clog << "insert(&sol, " << j << ", " << i << ");\n";
			insert(&sol, j, i);
			if (j == n - 1) j = 0;
			else j++;
		}
		insert(&sol, y, i);
	}

	print(sol);

	return 0;
}
