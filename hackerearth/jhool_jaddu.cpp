#include <iostream>
#include <cstring>
#include <string>

int main()
{
	int t;
	std::cin >> t;
	
	int hash[26] = {0};

	while (t--) {
		memset(hash, 0, sizeof(hash));
		std::string str;
		std::cin >> str;
		int i = 0;
		int l = str.length();
		while (i < l) {
			hash[str.at(i) - 'a'] = 1;
			i++;
		}

		str.clear();
		std::cin >> str;
		i = 0;
		l = str.length();
		while (i < l) {
			if (hash[str.at(i) - 'a']) {
				std::cout << "YES\n";
				break;
			}

			i++;
		}

		if (i == l)
			std::cout << "NO\n";
	}

	return 0;
}
