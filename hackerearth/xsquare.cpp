#include <iostream>
#include <cstring>

int hash(char ch)
{
	return ch - 97;
}

int main()
{
	int t;
	std::cin >> t;
	int a[26];
	while (t--) {
		char s[100];
		std::cin >> s;
		char* cstr = s;
		memset(a, 0, sizeof(a));
		while (*cstr) {
			int i = hash(*cstr);
			if (a[i]) {
				std::cout << "Yes" << std::endl;
				break;
			}

			a[i] = 1;
			cstr++;
		}

		if ('\0' == *cstr) std::cout << "No" << std::endl;
		
	}
	return 0;
}
