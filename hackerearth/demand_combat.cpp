#include <iostream>

int main()
{
	int t;
	std::cin >> t;
	while (t--) {
		int n, m;
		int l[100] = {0};
		int r[100] = {0};
		std::cin >> n >> m;
		for (int i = 0; i < n; i++) std::cin >> l[i];
		int flag = true;
		int *a, *b;
		for (int i = 0; i < m; i++) {
			a = flag ? l : r;
			b = !flag ? l : r;
			flag = !flag;
			b[0] = a[1] ? 1 : 0;
			b[n - 1] = a[n - 2] ? 1 : 0;
			for (int j = 1; j < n - 1; j++) {
				//if ((0 == j) && a[j + 1]) b[j] = 1;
				//else if ((n - 1 == j) && a[j - 1]) b[j] = 1;
				//else if ((0 < j) && (n - 1 > j) && (a[j - 1] && a[j + 1])) b[j] = 1;
				b[j] = (a[j - 1] && a[j + 1]) ? 1 : 0;
			}
		}
		a = flag ? l : r;
		for (int i = 0; i < n; i++) std::cout << a[i] << " ";
		std::cout << std::endl;
	}
	return 0;
}
