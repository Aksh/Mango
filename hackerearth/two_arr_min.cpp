#include <iostream>

#define ARR_SIZE	1000000

void generator(int A[ARR_SIZE], int B[ARR_SIZE], int n, int a, int b, int c)
{
	int i;
	A[0] = a*c;
	for(i=1 ; i<n ; i++)
	{
		A[i] = A[i-1]*a*b*c + A[i-1]*a*b + A[i-1]*a*c;
		A[i] = A[i]%1000000007;
	}
	B[0] = b*c;
	for(i=1 ; i<n ; i++)
	{
		B[i] = B[i-1]*b*c*a + B[i-1]*b*a + B[i-1]*b*c;
		B[i] = B[i]%1000000007;
	}
}

int main()
{
	int A[ARR_SIZE] = {0};
	int B[ARR_SIZE] = {0};
	int n = 3;
	int a, b, c;
	std::cin >> a >> b >> c;
	std::cin >> n;
	generator(A, B, n, a, b, c);
	for (int i = 0; i < n; i++) std::cout << A[i] << " ";
	std::cout << std::endl;
	for (int i = 0; i < n; i++) std::cout << B[i] << " ";
	std::cout << std::endl;
	for (int i = 0; i < n; i++) {
		int sum1 = A[0] + B[1];
		int sum2 = A[1] + B[0];
		std::cout << ((sum1 > sum2) ? sum2 : sum1) << std::endl;
	//}
	return 0;
}
