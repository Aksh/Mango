#include <iostream>
#include <queue>

int time_process(std::queue<int>& calling, int ideal[100], int n)
{
	int time = 0;
	for (int i = 0; i < n; i++) {
		int process = calling.front();
		while (process != ideal[i]) {
			calling.pop();
			calling.push(process);
			process = calling.front();
			time++;
		}
		calling.pop();
		time++;
	}

	return time;
}

int main()
{
	int n = 0;
	int ideal[100];
	std::queue<int> calling;
	std::cin >> n;
	int process = 0;
	for (int i = 0; i < n; i++) {
		std::cin >> process;
		calling.push(process);
	}
	for (int i = 0; i < n; i++) std::cin >> ideal[i];
	std::cout << time_process(calling, ideal, n) << std::endl;
	return 0;
}
