#include <iostream>
#include <pthread.h>
#include <unistd.h>

class demo {
public:
	demo() {std::cout << "demo::demo\n";}
	~demo() {std::cout << "demo::~demo\n";}
};

void* thread(void* args)
{
	demo d;
	for (;;) {
		std::cout << "thread instruction" << std::endl;
		sleep(1);
		//pthread_exit(NULL);
	}
}

int main()
{
	pthread_t tid;
	pthread_create(&tid, NULL, thread, NULL);
	sleep(1);
	pthread_cancel(tid);
	sleep(1);
	return 0;
}
