#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <pthread.h>

void* thread(void* args)
{
	//pthread_detach(pthread_self());
	for (int i = 1; i <= 50; i++) {
		std::cout << "thread instruction : " << i << std::endl;
	}
}

int main()
{
	pthread_t tid;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_create(&tid, &attr, thread, NULL);
	//sleep(1);
	return 0;
}
