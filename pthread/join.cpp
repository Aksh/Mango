#include <iostream>
#include <pthread.h>

void* thread(void* args)
{
	for (int i = 1; i <= 50; i++) {
		std::cout << "thread instruction : " << i << std::endl;
	}
}

int main()
{
	pthread_t tid;
	pthread_create(&tid, NULL, thread, NULL);
	pthread_join(tid, NULL);
	return 0;
}
