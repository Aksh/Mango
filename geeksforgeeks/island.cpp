#include <iostream>
#include <map>
#include <stack>

void find_islands(int a[][5], int i, int j, int **v, int m, int n)
{
	std::stack<std::pair<int, int> > s;
	s.push(std::pair<int, int> (i, j));
	while (!s.empty()) {
		std::tie(i, j) = s.top();
		s.pop();
		v[i][j] = 1;
		if ((i - 1 >= 0) && a[i - 1][j] && !v[i - 1][j]) {
			s.push(std::pair<int, int> (i - 1, j));
			find_islands(a, i - 1, j, v, m, n);
		}
		if ((j - 1 >= 0) && a[i][j - 1] && !v[i][j - 1]) {
			s.push(std::pair<int, int> (i, j - 1));
			find_islands(a, i, j - 1, v, m, n);
		}
		if ((i + 1 < m) && a[i + 1][j] && !v[i + 1][j]) {
			s.push(std::pair<int, int> (i + 1, j));
			find_islands(a, i + 1, j, v, m, n);
		}
		if ((j + 1 < n) && a[i][j + 1] && !v[i][j + 1]) {
			s.push(std::pair<int, int> (i, j + 1));
			find_islands(a, i, j + 1, v, m, n);
		}
	}
}

int num_of_islands(int a[][5], int m, int n)
{
	int **v = new int*[m];
	for (int i = 0; i < m; i++) {
		v[i] = new int[n];
		for (int j = 0; j < n; j++)
			v[i][j] = 0;
	}

	int count = 0;
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			if (a[i][j] && !v[i][j]) {
				count++;
				find_islands(a, i, j, v, m, n);
			}
		}
	}

	return count;
}

int main()
{
	int a[][5] = {1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1};

	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++)
			std::cout << a[i][j] << " ";
		std::cout << std::endl;
	}
	std::cout << std::endl;
	std::cout << "No of Islands : " << num_of_islands(a, 5, 5) << std::endl;

	return 0;
}
