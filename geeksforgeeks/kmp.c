#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int* calculate_lps(char* pat, int m)
{
	int i, j;
	int *lps = (int *) malloc(sizeof(int) * m);

	i = 1;
	j = 0;
	lps[0] = 0;
	while (i < m) {
		if (pat[i] == pat[j]) {
			j++;
			lps[i] = j;
			i++;
		}
		else {
			if (j != 0)
				j = lps[j - 1];
			else {
				lps[i] = 0;
				i++;
			}
		}
	}

	return lps;
}

void search(char* txt, int n, char* pat, int m)
{
	int i = 0;
	int j = 0;
	int* lps = calculate_lps(pat, m);

	while (i < n) {
		if (pat[j] == txt[i]) {
			i++;
			j++;
		}
		
		if (j == m) {
			printf ("pattern match at %d\n", i - j);
			j = lps[j - 1];
		}
		else if (i < n && pat[j] != txt[i]) {
			if (j != 0) {
				j = lps[j - 1];
			}
			else {
				i++;
			}
		}
	}
}

int main()
{
	char* txt = "akshaynikamsakshaynikam";
	char* pat = "aksh";
	
	printf("Text : %s\n", txt);
	printf("Pattern : %s\n", pat);
	search(txt, strlen(txt), pat, strlen(pat));

	return 0;
}
