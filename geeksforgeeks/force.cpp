#include <iostream>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	//A.B = a1*b1 + a2*b2 + a3*b3

	//A x B = (a2*b3 - a3*b2) i - (a1*b3 - b1*a3) j + (a1*b2 - a2*b1) k

	//|A|^2 = a1^2 + a2^2 + a3^2

	//If A.B = 0, then A and B are perpendicular.

	//If |A X B|^2 = 0, then A and B are parallel.
	while (test_cases) {
		int a1, a2, a3;
		int b1, b2, b3;
		std::cin >> a1 >> a2 >> a3;
		std::cin >> b1 >> b2 >> b3;
		int dot = (a1 * b1) + (a2 * b2) + (a3 * b3);
		int cross = ((a2 * b3) - (a3 * b2)) * ((a2 * b3) - (a3 * b2)) +
					((a1 * b3) - (b1 * a3)) * ((a1 * b3) - (b1 * a3)) + 
					((a1 * b2) - (a2 * b1)) * ((a1 * b2) - (a2 * b1));
		if (0 == dot) std::cout << 2 << std::endl;
		else if (0 == cross) std::cout << 1 << std::endl;
		else std::cout << 0 << std::endl;
		test_cases--;
	}

	return 0;
}
