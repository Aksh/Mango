#include <iostream>

int main()
{
	int a[100];
	int test_cases;
	std::cin >> test_cases;
	while (test_cases) {
		int n;
		std::cin >> n;

		for (int i = 0; i < n; i++)
			std::cin >> a[i];

		int k;
		std::cin >> k;
		for (int x = 0; x < n; x += k) {
			int i = x;
			int j = ((x + k - 1) < n) ? (x + k - 1) : (n - 1);
			while (i < j) {
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
				i++;
				j--;
			}
		}

		for (int i = 0; i < n; i++) std::cout << a[i] << " ";
		std::cout << std::endl;
		test_cases--;
	}
	return 0;
}
