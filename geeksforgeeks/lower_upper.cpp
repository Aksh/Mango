#include <iostream>

int main()
{
	char str[50] = {0};
	int test_cases;
	std::cin >> test_cases;
	while (test_cases) {
		std::cin >> str;
		int i = 0;
		while (str[i]) {
			str[i] -= 32;
			i++;
		}
		std::cout << str << std::endl;
		test_cases--;
	}
	return 0;
}
