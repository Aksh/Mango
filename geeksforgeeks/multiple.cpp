#include <iostream>
#include <sstream>
#include <cstdlib>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	std::string sum = "0";
	while (test_cases) {
		int n;
		std::cin >> n;
		for (int i = 1; i < n; i++) {
			if ((0 == i % 3) || (0 == i % 7)) {
				std::stringstream convert;
				convert << (atoi(sum.c_str()) + i);
				sum = convert.str();
			}
		}
		
		std::cout << sum << std::endl;
		sum = "0";
		test_cases--;
	}

	return 0;
}
