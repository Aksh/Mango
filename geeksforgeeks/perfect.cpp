#include <iostream>

int fact(int n)
{
	if (n == 0 || n == 1) return 1;
	return n * fact(n - 1);
}

int main()
{
	int test_cases;
	std::cin >> test_cases;
	while (test_cases) {
		int n;
		std::cin >> n;
		int p = n;
		int sum = 0;
		while (p) {
			int r = p % 10;
			sum += fact(r);
			p = p / 10;
		}

		if (sum == n)
			std::cout << "Perfect" << std::endl;
		else
			std::cout << "Not Perfect" << std::endl;

		test_cases--;
	}

	return 0;
}
