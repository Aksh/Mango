#include <stdio.h>

void reverse_rows(int a[][4], int m, int n)
{
	int i, j, k, t;

	for (i = 0; i < m; i++) {
		for (j = 0, k = n - 1; j < k; j++, k--) {
			t = a[i][k];
			a[i][k] = a[i][j];
			a[i][j] = t;
		}
	}
}

void transpose(int a[][4], int m, int n)
{
	int i, j, t;

	for (i = 0; i < m; i++) {
		for (j = i; j < n; j++) {
			t = a[i][j];
			a[i][j] = a[j][i];
			a[j][i] = t;
		}
	}
}

void rotate_90_clock(int a[][4], int m, int n)
{
	transpose(a, m, n);
	reverse_rows(a, m, n);
}

int main()
{
	int a[][4] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
	int i, j;
	
	rotate_90_clock(a, 4, 4);

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++)
			printf("%d ", a[i][j]);
		printf("\n");
	}

	return 0;
}
