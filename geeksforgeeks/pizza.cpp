#include <iostream>
#include <algorithm>

int pizza(int a[1000], int b[1000],int n)
{
	int counter = 0;
	for (int i = 0; i < n; i++)
		if (std::binary_search(a, a + n, b[i]))
			counter++;
	return counter;
}

int main()
{
	int test_cases;
	std::cin >> test_cases;
	int a[10] = {0};
	int b[10] = {0};
	int n = 10;
	while (test_cases) {
		for (int i = 0; i < n; i++) std::cin >> a[i];
		for (int i = 0; i < n; i++) std::cin >> b[i];
		std::sort(a, a + n);
		std::cout << pizza(a, b, n) << std::endl;
		test_cases--;
	}

	return 0;
}
