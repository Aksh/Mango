#include <iostream>

void sort(int a[100], int n)
{
	for (int i = 1; i < n; i++) {
		int temp = a[i];
		int j;
		for (j = i - 1; temp < a[j] && j >= 0; j--) {
			a[j + 1] = a[j];
		}

		a[j + 1] = temp;
	}
}

void move(int arr[200], int n, int i)
{
	int temp = arr[n];
	int j = n;
	while (j > i + 1) {
		arr[j] = arr[j - 1];
		j--;
	}

	arr[j] = temp;
}

int main()
{
	int test_cases;
	int a[200] = {0};
	std::cin >> test_cases;
	while (test_cases) {
		int n;
		std::cin >> n;
		for (int i = 0; i < n; i++)
			std::cin >> a[i];
		sort(a, n);
		int i = 0;
		while (i < n) {
			move(a, n - 1, i);
			i += 2;
		}

		for (int i = 0; i < n; i++)
			std::cout << a[i] << " ";
		std::cout << std::endl;	
		test_cases--;
	}

	return 0;
}
