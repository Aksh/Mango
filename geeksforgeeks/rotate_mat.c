#include <stdio.h>

void spiral(int a[][4], int m, int n)
{
	int k = 0; // start index of remaining rows
	int l = 0; // start index of remaining columns
	int i = 0; // iterator
			   // m = end index of remaining rows
			   // n = end index of remaining column
	int cur;
	int prev;
	while ((k < m) && (l < n)) {
		if (((k + 1) == m) || ((l + 1) == n))
			break;
		prev = a[k + 1][i];
		for (i = l; i < n; i++) {
			//printf("%d ", a[k][i]);
			cur = a[k][i];
			a[k][i] = prev;
			prev = cur;
		}
		k++;

		for (i = k; i < m; i++) {
			//printf("%d ", a[i][n - 1]);
			cur = a[i][n - 1];
			a[i][n - 1] = prev;
			prev = cur;
		}
		n--;
		
		if (k < m) {
			for (i = n - 1; i >= l; i--) {
				//printf("%d ", a[m - 1][i]);
				cur = a[m - 1][i];
				a[m - 1][i] = prev;
				prev = cur;
			}
			m--;
		}

		if (l < n) {
			for (i = m - 1; i >= k; i--) {
				//printf("%d ", a[i][l]);
				cur = a[i][l];
				a[i][l] = prev;
				prev = cur;
			}
			l++;
		}
	}
}

int main()
{
	int i = 0;
	int j = 0;
	int a[4][4] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
	spiral(a, 4, 4);
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++)
			printf("%d ", a[i][j]);
		printf("\n");
	}

	return 0;
}
