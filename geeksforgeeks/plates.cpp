#include <iostream>
#include <map>

std::map<int, std::pair<int, int>> moves;
static int move = 0;

void get_nth_move(int plates, int a, int b, int c, int n)
{
	if (1 == plates) {
		moves[++move] = std::pair<int, int>(a, b);
		if (move == n)
			throw n;
	}
	else {
		get_nth_move(plates - 1, a, c, b, n);
		moves[++move] = std::pair<int, int>(a, b);
		if (move == n)
			throw n;
		get_nth_move(plates - 1, c, b, a, n);
	}
}

int main()
{
	int test_cases;
	std::cin >> test_cases;
	int num = 0;
	int n;
	while (test_cases) {
		std::cin >> num >> n;
		move = 0;
		moves.clear();
		try {
			get_nth_move(num, 1, 3, 2, n);
		}
		catch (int i) {
			std::cout << moves[i].first << " " << moves[i].second << std::endl;
		}
		test_cases--;
	}

	return 0;
}
