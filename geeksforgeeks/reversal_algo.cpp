#include <iostream>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	int a[200] = {0};
	while (test_cases) {
		int n;
		std::cin >> n;
		for (int i = 0; i < n; i++) std::cin >> a[i];
		int d;
		std::cin >> d;

		while (d) {
			int temp = a[0];
			int i = 0;
			while (i < n) {
				a[i] = a[i + 1];
				i++;
			}
			a[n - 1] = temp;
			d--;
		}

		for (int i = 0; i < n; i++) std::cout << a[i] << " ";
		std::cout << std::endl;
		test_cases--;
	}
	return 0;
}
