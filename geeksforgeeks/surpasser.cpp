#include <iostream>
//#include <algorithm>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	int n;
	int a[500] = {0};
	while (test_cases) {
		std::cin >> n;
		for (int i = 0; i < n; i++) std::cin >> a[i];
		for (int i = 0; i < n; i++) {
			int j = i;
			int count = 0;
			while (j < n) {
				if (a[j] > a[i])
					count++;
				j++;
			}
			std::cout << count << " ";
		}
		std::cout << std::endl;
		test_cases--;
	}

	return 0;
}
