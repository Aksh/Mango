#include <iostream>

void swap(int& a, int& b)
{
	int temp = a;
	a = b;
	b = temp;
}

void rotate(int a[50][50], int n)
{
	for (int i = 0; i < n; i++) {
		int j = 0;
		int k = n - 1;
		while (k > j) {
			swap(a[i][j], a[i][k]);
			j++;
			k--;
		}
	}
	
	for (int i = 0; i < n*2 - 1; i++) {
		int j = ((i < n) ? 0 : (n - 1));
		int k = ((i / n) ? (i % n + 1) : i);
		if (j > k) swap(j, k);
		while (j < k) {
			swap(a[j][k], a[k][j]);
			j++;
			k--;
		}
	}
}

int main()
{
	int test_cases;
	int a[50][50] = {0};
	std::cin >> test_cases;
	while (test_cases) {
		int n;
		std::cin >> n;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				std::cin >> a[i][j];
	
		rotate(a, n);
		std::cout << std::endl;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++)
				std::cout << a[i][j] << " ";
			//std::cout << std::endl;
		}
		std::cout << std::endl;	
		test_cases--;
	}

	return 0;
}
