#include <iostream>
#include <cstring>

void sort(int a[100], int n)
{
	for (int i = 1; i < n; i++) {
		int temp = a[i];
		int j;
		for (j = i - 1; temp > a[j] && j >= 0; j--) {
			a[j + 1] = a[j];
		}

		a[j + 1] = temp;
	}
}

int min_value(char str[50], int k)
{
	int count[58] = {0};
	int i = 0;
	if (k >= strlen(str)) return 0;
	while (str[i]) {
		count[str[i] - 65]++;
		i++;
	}

	sort(count, 58);

	int value = 0;
	for (i = 0; i < 58; i++) {
		std::cout << count[i] << " ";
		if (0 == k)
			value += count[i] * count[i];
		else {
			if (count[i] > k) {
				value += (count[i] - k) * (count[i] - k);
				k = 0;
			}
			else {
				if (count[i + 1] > 1) {
 	 				k = k - count[i] + 1;
					value++;
				}
				else
					k = k - count[i];
			}
		}
	}
	return value;
}

int main()
{
	int test_cases;
	char str[50] = {0};
	std::cin >> test_cases;
	while (test_cases) {
		int k;
		std::cin >> str;
		std::cin >> k;
		std::cout << min_value(str, k) << std::endl;
		test_cases--;
	}

	return 0;
}
