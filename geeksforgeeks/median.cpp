#include <iostream>

void sort(int a[100], int n)
{
	for (int i = 1; i < n; i++) {
		int temp = a[i];
		int j;
		for (j = i - 1; temp < a[j] && j >= 0; j--) {
			a[j + 1] = a[j];
		}

		a[j + 1] = temp;
	}
}

int main()
{
	int test_cases;
	int a[100] = {0};
	std::cin >> test_cases;
	while (test_cases) {
		int n;
		std::cin >> n;
		for (int i = 0; i < n; i++)
			std::cin >> a[i];
		sort(a, n);
		if (n % 2) {
			std::cout << a[((n + 1) / 2) - 1] << std::endl;
		}
		else {
			std::cout << (int) ((a[((n + 1) / 2) - 1] + a[(n + 1) / 2]) / 2) << std::endl;
		}
		test_cases--;
	}

	return 0;
}
