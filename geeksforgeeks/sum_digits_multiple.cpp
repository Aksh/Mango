#include <iostream>
#include <sstream>
#include <cstdlib>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	int sum = 0;
	while (test_cases) {
		int n;
		std::cin >> n;
		int num = n;
		while (n) {
			int r = n % 10;
			sum += r;
			n = n / 10;
		}
		if (num % sum)
			std::cout << "No" << std::endl;
		else
			std::cout << "Yes" << std::endl;
		sum = 0;
		test_cases--;
	}

	return 0;
}
