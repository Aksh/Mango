#include <iostream>
#include <limits>
#include <cstdio>
#include <string>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	char c;
	std::string num;
	while (test_cases) {
		int r = 0;
		std::cin >> num;
		int i = 0;
		while (i < num.size()) {
			c = num.at(i);
			r = (r * 10 + c - 48) % 7;
			i++;
		}
		std::cout << r << std::endl;
		test_cases--;
	}

	return 0;
}
