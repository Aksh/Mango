#include <stdio.h>
#include <stdlib.h>

int min(int a, int b, int c)
{
	return (a < b ? (a < c ? a : c) : (b < c ? b : c));
	//if ((a > b) && (a > c))
	//	return a;
	//if ((b > a) && (b > c))
	//	return b;
	//if ((c > a) && (c > b))
	//	return c;
}

void maxSubSquare(int m[][5], int r, int c)
{
	int **s = NULL;
	int i, j, max_s, max_i, max_j;

	s = (int **) malloc(sizeof(int*) * r);
	for (i = 0; i < r; i++) {
		s[i] = (int*) malloc(sizeof(int) * c);
	}

	for (i = 0; i < r; i++)
		s[i][0] = m[i][0];

	for (i = 0; i < c; i++)
		s[0][i] = m[0][i];

	for (i = 1; i < r; i++) {
		for (j = 1; j < c; j++) {
			if (m[i][j])
				s[i][j] = min(s[i - 1][j], s[i][j - 1], s[i - 1][j - 1]) + 1;
			else
				s[i][j] = 0;
		}
	}

	max_s = 0;
	for (i = 0; i < r; i++)
		for (j = 0; j < c; j++)
			if (max_s < s[i][j]) {
				max_s = s[i][j];
				max_i = i;
				max_j = j;
			}
	
	printf("%d*%d square, right bottom corner at (%d, %d)\n", max_s, max_s, max_i, max_j);
}

int main()
{
	int a[][5] = {0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0};
	maxSubSquare(a, 6, 5);

	return 0;
}
