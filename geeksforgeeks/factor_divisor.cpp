#include <iostream>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	while (test_cases) {
		int n;
		std::cin >> n;
		int counter = 0;
		for (int i = 1; i <= n/2; i++) {
			if (0 == n % i) {
				if (0 == i % 3)
					counter++;
			}
		}
		if (0 == n % 3)
			counter++;
		std::cout << counter << std::endl;
		test_cases--;
	}

	return 0;
}
