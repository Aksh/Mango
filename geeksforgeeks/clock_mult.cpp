#include <iostream>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	while (test_cases) {
		int n1, n2;
		std::cin >> n1 >> n2;
		std::cout << (n1 * n2) % 12 << std::endl;
		test_cases--;
	}
	return 0;
}
