#include <iostream>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	int n;
	int k;
	int count = 0;
	int a[500] = {0};
	while (test_cases) {
		std::cin >> n >> k;
		count = 0;
		for (int i = 0; i < n; i++) std::cin >> a[i];
		for (int i = 0; i < n; i++) {
			count += a[i]/k;
			if (a[i] % k)
				count++;
		}
		std::cout << count << std::endl;
		test_cases--;
	}

	return 0;
}
