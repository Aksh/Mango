#include <iostream>
#include <algorithm>

void search(int st, int en, int m, int& t)
{
	if (st <= en) {
		int mid = (st + en) / 2;
		t++;
		if (mid == m)
			return;
		else if (mid < m)
			search(st, mid - 1, m, t);
		else
			search(mid + 1, en, m, t);
	}
}

int binary_search(int n, int m, int s) {
	int t = 0;
	search(0, n, m, t);
	return t * s;
}

int main()
{
	int test_cases;
	std::cin >> test_cases;
	int n, m, g, s;
	while (test_cases) {
		std::cin >> n >> m >> g >> s;
		int t = binary_search(n, m, s);
		if ((g * m) > t) std::cout << 2 << std::endl;
		else std::cout << 1 << std::endl;
		test_cases--;
	}

	return 0;
}
