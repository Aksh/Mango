#include <iostream>
#include <sstream>
#include <cstdlib>

bool isprime(int n)
{
	if (1 == n)
		return false;
	for (int i = 2; i <= (n / 2); i++)
		if (0 == (n % i))
			return false;
	return true;
}

int main()
{
	int test_cases;
	std::cin >> test_cases;
	int sum = 0;
	while (test_cases) {
		int n;
		std::cin >> n;
		while (n) {
			int r = n % 10;
			if (isprime(r))
				sum += r;
			n = n / 10;
		}
		std::cout << sum << std::endl;
		sum = 0;
		test_cases--;
	}

	return 0;
}
