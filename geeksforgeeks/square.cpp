#include <iostream>
#include <math.h>

int main()
{
	int test_cases;
	std::cin >> test_cases;
	while (test_cases) {
		int x1, y1, first;
		int x2, y2, second;
		int x3, y3, third;
		int x4, y4, fourth;
		std::cin >> x1 >> y1
				>> x2 >> y2
				>> x3 >> y3
				>> x4 >> y4;
		first = sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
		second = sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3));
		//fourth = sqrt((x4 - x3) * (x4 - x3) + (y4 - y3) * (y4 - y3));
		third = sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
		int d = -1;
		if (first == second && (2 * first == third)) {
			d = sqrt(third);
		}
		else if (first == third) {
			d = sqrt(second);
		}
		else if (second == third) {
			d = sqrt(first);
		}
		else
			std::cout << 0 << std::endl;
		if ((d == (int)(sqrt(2) * first)) || (d == (int)(sqrt(2) * second)) || (d == (int)(sqrt(2) * third)))
			std::cout << 1 << std::endl;
		else
			std::cout << 0 << std::endl;
		test_cases--;
	}
	return 0;
}
