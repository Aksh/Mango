#include <stdio.h>
#include <stdlib.h>

int lis(int arr[], int n)
{
	int *l = NULL;
	int max = 0;
	int i, j;

	l = (int *) malloc(sizeof(int) * n);
	for (i = 0; i < n; i++)
		l[i] = 1;
	
	for (i = 1; i < n; i++) {
		for (j = 0; j < i; j++) {
			if ((arr[j] < arr[i]) && (l[i] < (l[j] + 1)))
				l[i] = l[j] + 1;
		}
	}

	for (i = 0; i < n; i++) {
		if (max < l[i]) {
			max = l[i];
		}
	}

	return max;
}

int main()
{
	int arr[10];
	int n;
	int i;

	scanf("%d", &n);
	for (i = 0; i < n; i++) {
		scanf("%d", &arr[i]);
	}

	printf("LIS : %d\n", lis(arr, n));

	return 0;
}
