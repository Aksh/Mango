#include <stdio.h>

void spiral(int a[][4], int m, int n)
{
	int k = 0; // start index of remaining rows
	int l = 0; // start index of remaining columns
	int i = 0; // iterator
			   // m = end index of remaining rows
			   // n = end index of remaining column
	
	while ((k < m) && (l < n)) {
		for (i = l; i < n; i++)
			printf("%d ", a[k][i]);
		k++;

		for (i = k; i < m; i++)
			printf("%d ", a[i][n - 1]);
		n--;
		
		if (k < m) {
			for (i = n - 1; i >= l; i--)
				printf("%d ", a[m - 1][i]);
			m--;
		}

		if (l < n) {
			for (i = m - 1; i >= k; i--)
				printf("%d ", a[i][l]);
			l++;
		}
	}
}

int main()
{
	int a[4][4] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
	spiral(a, 4, 4);
	printf("\n");

	return 0;
}
