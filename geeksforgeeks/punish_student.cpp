#include <iostream>

void swap(int& a, int& b)
{
	int temp = a;
	a = b;
	b = temp;
}

int can_punish(int a[1000], int m[1000], int n, float avg)
{
	int s[1000] = {0};
	for (int i = 0; i < n - 1; i++) {
		for (int j = i; j < n - 1; j++) {
			if (a[j] > a[j + 1]) {
				s[a[j]]++;
				s[a[j + 1]]++;
				swap(a[j], a[j + 1]);
				swap(m[j], m[j + 1]);
			}
		}
	}

	int sum = 0;
	for (int i = 0; i < n; i++) sum += m[i] - s[a[i]];
	if (avg > (sum / n)) return 0;
	return 1;
}

int main()
{
	int test_cases;
	int r[1000] = {0};
	int m[1000] = {0};
	std::cin >> test_cases;
	int n;
	float a;
	while (test_cases) {
		std::cin >> n;
		std::cin >> a;
		for (int i = 0; i < n; i++) std::cin >> r[i];
		for (int i = 0; i < n; i++) std::cin >> m[i];
		std::cout << can_punish(r, m, n, a) << std::endl;
		test_cases--;
	}

	return 0;
}
