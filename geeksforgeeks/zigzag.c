#include <stdio.h>

void zigzag(int a[][4], int row, int col)
{
	int i, j, k; // iterator
	
	// for upper diagonal
	for (i = 0; i < row; i++) {
		k = i;
		for (j = 0; (j < col) && (k >= 0); j++, k--)
			printf("%d ", a[k][j]);
		printf("\n");
	}

	// for lower diagonal
	for (i = 0; i < col - 1; i++) {
		k = row - 1;
		for (j = i + 1; (j < col) && (k > 0); j++, --k)
			printf("%d ", a[k][j]);
		printf("\n");
	}
}

int main()
{
	int a[][4] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
	zigzag(a, 5, 4);

	return 0;
}
