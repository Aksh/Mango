#include <iostream>
#include <cmath>
#include <cstring>

void decimal_to_binary(int n)
{
	if (0 == n) return;
	decimal_to_binary(n / 2);
	std::cout << n % 2;
}

void binary_to_decimal(int n)
{
	int num = 0;
	int i = 0;
	while (n) {
		num += (n % 10 )* (pow(2, i));
		n = n / 10;
		i++;
	}

	std::cout << num;
}

void decimal_to_hexadecimal(int n)
{
	if (0 == n) return;
	decimal_to_hexadecimal(n / 16);
	if ((n % 16) < 10) std::cout << n % 16;
	else std::cout << (char) ((n % 16) + 55);
}

void hexadecimal_to_decimal(const char* str)
{
	int num = 0;
	int i = strlen(str) - 1;
	int len = i;
	while (i >= 0) {
		int digit = str[i] > '9' ? (str[i] - 55) : (str[i] - 48);
		num += digit * (pow(16, len - i));
		i--;
	}

	std::cout << num;
}

int main()
{
	int test_cases;
	std::cin >> test_cases;
	int a, b, c;
	std::string d;
	while (test_cases) {
		std::cin >> a >> b >> c >> d;
		decimal_to_binary(a);
		std::cout << " ";
		binary_to_decimal(b);
		std::cout << " ";
		decimal_to_hexadecimal(c);
		std::cout << " ";
		hexadecimal_to_decimal(d.c_str());
		std::cout << std::endl;
		test_cases--;
	}

	return 0;
}
