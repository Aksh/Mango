#include <iostream>

void sort(int a[100], int n)
{
	for (int i = 1; i < n; i++) {
		int temp = a[i];
		int j;
		for (j = i - 1; temp < a[j] && j >= 0; j--) {
			a[j + 1] = a[j];
		}

		a[j + 1] = temp;
	}
}

int main()
{
	int test_cases;
	int a[1000] = {0};
	int b[1000] = {0};
	std::cin >> test_cases;
	while (test_cases) {
		int n;
		std::cin >> n;
		int sum = 0;
		for (int i = 0; i < n; i++)
			std::cin >> a[i];
		for (int i = 0; i < n; i++)
			std::cin >> b[i];
		sort(a, n);
		sort(b, n);
		
		for (int i = 0 ; i < n; i++)
			sum += a[i] * b[i];

		std::cout << sum << std::endl;

		test_cases--;
	}

	return 0;
}
