#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

void err_exit(const char* msg)
{
	printf("%s : %s\n", msg, strerror(errno));
	exit(-1);
}

void child_fun(int fd)
{
	char buff[1024] = {0};
	char* cstr = &buff[0];
	int ret = 0;
	char ch;
	ret = read(fd, &buff, sizeof(buff));
	printf("Child process processing the input\n");
	while (0 != *cstr) {
		ch = toupper(*cstr);
		putchar(ch);
		cstr++;
	}
}

void parent_fun(int fd)
{
	char buff[1024] = {0};
	printf("Parent process waiting for input : \n");
	read(0, &buff, sizeof(buff));
	write(fd, buff, sizeof(buff));
}

int main()
{
	int fd[2] = {0};
	int ret;
	ret = pipe(fd);	// fd[0] - for read, fd[1] - for write
	if (-1 == ret)
		err_exit("pipe");

	switch (fork()) {
		case -1:
			err_exit("fork");
			break;

		case 0:
			close(fd[1]);	// child process should close write descriptor
			child_fun(fd[0]);
			break;

		default:
			close(fd[0]);	// parent process should close read descriptor
			parent_fun(fd[1]);
			break;
	}

	return 0;
}
