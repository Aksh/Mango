#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>

#include <iostream>

int main()
{
	int shmfd = shm_open("test", O_CREAT | O_RDWR, 0666);
	ftruncate(shmfd, 1024);
	char* addr = (char *)mmap(0, 1024, PROT_WRITE, MAP_SHARED, shmfd, 0);
	mlock(addr, 1024);
	char buff[] = "Inter Process Communication usinf Shared Memory\n";
	write(shmfd, buff, sizeof(buff));
	int i;
	std::cin >> i;
	munmap(addr, 1024);
	close(shmfd);
	shm_unlink("test");
	return 0;
}
