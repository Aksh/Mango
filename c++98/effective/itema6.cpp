#include <iostream>

class uncopyable {
	private:
		uncopyable(const uncopyable&);
		uncopyable& operator =(const uncopyable&);

	public:
		uncopyable() {}
		~uncopyable() {}
};

class demo : public uncopyable {
	public:
		friend void fun(demo&, const demo&);
};

void fun(demo& d1, const demo& d2)
{
	d1 = d2;	// error !!!
}

int main()
{
	demo d1, d2;
	fun(d1, d2);
	//d1 = d2;	// error !!!
	return 0;
}
