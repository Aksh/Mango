#include <iostream>

class demo {
	public:
		demo& operator=(const demo& rhs)
		{
			//
			// do something
			//
			return *this;
		}
};

int main()
{
	demo d1, d2, d3;
	d1 = d2 = d3;
	return 0;
}
