#include <iostream>
#include <memory>
#include <tr1/memory>

class demo {
public:
	demo() {std::cout << "demo::demo()\n";}
	~demo() {std::cout << "~demo::demo()\n";}
};

void fun()
{
	std::cout << "\n***auto_ptr***\n";
	std::auto_ptr<demo> d(new demo());
	std::auto_ptr<demo> d1(d);	// d1 points to the object and d is null
	std::auto_ptr<demo> d2;
	d2 = d1;					// d2 points to the object and d1 is null
	std::cout << "d : " << d.get() << std::endl;
	std::cout << "d1 : " << d1.get() << std::endl;
	std::cout << "d2 : " << d2.get() << std::endl;
	return;
}

void gun()
{
	std::cout << "\n***tr1::shared_ptr***\n";
	std::tr1::shared_ptr<demo> d(new demo());
	std::tr1::shared_ptr<demo> d1(d);	// d1 and d both are pointing to the object with reference count 2
	std::tr1::shared_ptr<demo> d2;
	d2 = d1;							// d2, d1 and all are pointing to the object with reference count 3

	std::cout << "d : " << d.get() << std::endl;
	std::cout << "d1 : " << d1.get() << std::endl;
	std::cout << "d2 : " << d2.get() << std::endl;
	return;
}

std::tr1::shared_ptr<demo> shared_fun()
{
	std::tr1::shared_ptr<demo> iptr(new demo);
	return iptr;
}

std::auto_ptr<demo> auto_fun()
{
	std::auto_ptr<demo> ptr(new demo());
	return ptr;
}

int main()
{
	fun();
	gun();
	std::cout << "\nbefore shared_fun\n";
	std::tr1::shared_ptr<demo> d1 = shared_fun();
	std::cout << "d1 : " << d1.get() << std::endl;
	std::cout << "after shared_fun\n";
	std::cout << "\nbefore auto_fun\n";
	std::auto_ptr<demo> d2 = auto_fun();
	std::cout << "d2 : " << d2.get() << std::endl;
	std::cout << "after auto_fun\n";
	return 0;
}
