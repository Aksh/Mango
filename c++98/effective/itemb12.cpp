#include <iostream>

class base {
private:
	std::string name;

	/*
	 * Dont try to implement one of the copying function in terms of the others.
	 * Instead, put common functionality in third function that both call
	 */
	void init(const base& b)
	{
		name = b.name;
	}

public:
	base(std::string str) : name(str) {}
	base(const base& b) {init(b);}
	base& operator=(const base& rhs) {init(rhs);}
	~base() {}
	void show()
	{
		std::cout << name << std::endl;
	}
};

class der : public base {
private:
	int no;

public:
	der(std::string str, int i) : base(str), no(i) {}

	/*
	 * Copying functions should be sure to copy all of an objects data members 
	 * and all of its base class part.
	 */
	der(const der& rhs) : base(rhs), no(rhs.no) {}
	der& operator=(const der& rhs)
	{
		base::operator=(rhs);
		no = rhs.no;
	}
	~der() {}
	void show()
	{
		std::cout << no << " : ";
		base::show();
	}
};

int main()
{
	der d1("Aksh", 1);
	der d2("Pallu", 2);
	der d3 = d2;
	der d4("", 0);
	d4 = d1;
	std::cout << "d1 : ";
	d1.show();
	std::cout << "d2 : ";
	d2.show();
	std::cout << "d3 : ";
	d3.show();
	std::cout << "d4 : ";
	d4.show();
	return 0;
}
