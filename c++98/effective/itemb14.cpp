#include <iostream>
#include <tr1/memory>

void unlock(int* i) {std::cout << "unlocking the mutex : " << *i << "\n";}

class Lock {
private:
	std::tr1::shared_ptr<int> mutex;

public:
	explicit Lock(int i) : mutex(new int(i), unlock) 
	{
		std::cout << "create << lock the mutex : " << *(mutex.get()) << std::endl;
	}

	Lock(const Lock& l) : mutex(l.mutex)
	{
		std::cout << "copy << lock the mutex : " << *(mutex.get()) << std::endl;
	}
};

int main()
{
	std::cout << "Before critical section\n";
	{
		Lock l1(2);
		Lock l2 = l1;
	}
	std::cout << "After critical section\n";
	return 0;
}
