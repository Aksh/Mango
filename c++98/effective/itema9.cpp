#include <iostream>

class base {
	public:
		base() {
			std::cout << "base::base()\n";
			fun();
		}

		~base() {
			std::cout << "base::~base()\n";
		}

		virtual void fun()
		{
			std::cout << "base::fun()\n";
		}
};

class der : public base {
	public:
		der()
		{
			std::cout << "der::der()\n";
		}

		~der()
		{
			std::cout << "der::~der()\n";
		}

		void fun()
		{
			std::cout << "der::fun()\n";
		}
};

int main()
{
	der d;
	return 0;
}
