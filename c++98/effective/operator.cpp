#include <iostream>

class demo {
	private:
		int* ptr;

	public:
		demo(int n = 10) {ptr = new int[n]; for (int i = 0; i < n; i++) ptr[i] = i*10;}
		int& operator [](int index) {return ptr[index];}
		const int& operator [](int index) const{return ptr[index];}
};

int main()
{
	demo d;
	std::cout << d[1] << ' ' << d[4] << std::endl;
	d[1] = 23;
	std::cout << d[1] << ' ' << d[4] << std::endl;
	const demo d1;
	std::cout << d1[1] << ' ' << d1[4] << std::endl;
	//d1[1] = 23;	// error
	//std::cout << d1[1] << ' ' << d1[4] << std::endl;
	return 0;
}
