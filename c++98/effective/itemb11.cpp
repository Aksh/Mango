#include <iostream>

class demo {
public:
	demo& operator=(const demo& rhs)
	{
		if (this == &rhs) return *this;
		//
		// implement = operator with exception safety
		//
		return *this;
	}
};

int main()
{
	demo d1;
	d1 = d1;
	return 0;
}
