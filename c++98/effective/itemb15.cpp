#include <iostream>

class resource {
	int i;

public:
	resource(int ii) : i(ii) {}
	void fun() {std::cout << "resource::fun(" << i << ")\n";}
};

class demo {
	resource r;

public:
	demo() : r(12) {}
	resource& get() {return r;}
};

int main()
{
	demo d;
	resource r = d.get();
	r.fun();
	return 0;
}
