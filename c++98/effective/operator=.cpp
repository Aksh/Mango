#include <iostream>

class base {
	private:
	//public:
		base& operator =(const base& b)
		{
			std::cout << "base::operator=\n";
		}
};

class der : public base {
	public:
		//der& operator =(const der& d)
		//{
		//	std::cout << "der::operator=\n";
		//}
};

int main()
{
	der d1, d2;
	d1 = d2;	// error !!!
	return 0;
}
