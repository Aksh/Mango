#include <iostream>

class demo {
	public:
		explicit demo(int i) {
			std::cout << "demo constructor : " << i << std::endl;
		}
};

void fun(demo d) {
	std::cout << "fun\n";
}

int main()
{
	demo d(10);
	fun(d);
	// fun(11); // error !!!
	fun(demo(11));
	return 0;
}
