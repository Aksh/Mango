#include <iostream>

class demo {
private:
	static int counter;
	int id;

public:
	demo() {counter++; id = counter; std::cout << "demo::demo(" << id << ")\n";}
	~demo() {std::cout << "demo::~demo(" << id << ")\n";}
};

int demo::counter = 0;

int main()
{
	demo* dptr = new demo[5];
	//delete dptr;	// error: undefined behaviour
	delete []dptr;
	return 0;
}
