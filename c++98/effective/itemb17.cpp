#include <iostream>
#include <tr1/memory>

class demo {
public:
	demo() {std::cout << "demo::demo()\n";}
	~demo() {std::cout << "demo::~demo()\n";}
};

int get_priority() {return 11;}

void fun(std::tr1::shared_ptr<demo> pd, int priority)
{
	;
}

int main()
{
	std::tr1::shared_ptr<demo> pd(new demo);
	fun(pd, get_priority());
	return 0;
}
