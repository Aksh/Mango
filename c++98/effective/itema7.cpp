#include <iostream>

class base {
	public:
		base() {}
		virtual
		~base() {std::cout << "base::~base()\n";}
};

class der : public base {
	public:
		der() {}
		~der() {std::cout << "der::~der()\n";}
};

int main()
{
	base* bptr = new der();
	delete bptr;
	return 0;
}
