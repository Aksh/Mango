// prefer const and inline to #define

#include <iostream>
#define max(a, b) ((a) > (b) ? (a) : (b))

int main()
{
	int a = 5, b = 0;
	max(++a, b);	// a is incremented twice
	std::cout << a << std::endl;

	return 0;
}
