// use the same form in corresponding uses of new and delete

int main()
{
	int* i1 = new int;
	int* i2 = new int[3];

	delete i1; // if u'd use delete [] i1 its behaviour will be undefined
	delete [] i2; // if u'd use delete i2 its behaviour will be undefined

	return 0;
}
