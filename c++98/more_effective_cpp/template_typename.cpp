/*
 * using 'typename' to say it's a type,
 * and not something other than a type
 */

#include <iostream>

template <class T>
class x {
	// without typename, you should get an error
	typename T::id i;
public:
	void fun() {
		i.gun();
	}
};

class y {
public:
	class id {
		public:
			void gun() {std::cout << "y::id::gun()\n";}
	};
};

int main()
{
	x<y> xy;
	xy.fun();
	return 0;
}
