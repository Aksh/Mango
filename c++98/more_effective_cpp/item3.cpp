// prefer new and delete to malloc and free

#include <iostream>
#include <cstdlib>

class demo {
	public:
	demo() {
		std::cout << "demo::constructor\n";
	}

	~demo() {
		std::cout << "~demo::destructor\n";
	}
};

int main()
{
	demo* d1 = static_cast<demo*> (malloc(5*sizeof(demo))); // constructors will get called for these objects
	demo* d2 = new demo[5]; // constructor called

	free(d1); // destructor will not get called for these objects
	delete [] d2; // destructor called

	return 0;
}
