#include <iostream>

template<class T, int sz = 1>
class PWrap {
	T* ptr;
	public:
		PWrap() {
			ptr = new T[sz];
			std::cout << "PWrap::PWrap()\n";
		}

		~PWrap() {
			delete[] ptr;
			std::cout << "PWrap::~PWrap()\n";
		}

		T& operator[] (int i) {
			if (i >= 0 and i < sz) return ptr[i];
			throw "OutOfRangeException";
		}
};

class resource {
	public:
		resource() {std::cout << "resource::resource()\n";}
		~resource() {std::cout << "resource::~resource()\n";}
};

class useResource {
	PWrap<resource, 3> r;
	public:
		useResource(int i) {
			std::cout << "useResource::useResource()\n";
			//throw 4;
		}
		
		~useResource() {
			std::cout << "useResource::~useResource()\n";
		}

		void getResource(int i) {
			r[i];
		}
};

int main() {
	try {
		useResource r(3);
		std::cout << "r.getResource(0) : "; r.getResource(0); std::cout << std::endl;
		std::cout << "r.getResource(1) : "; r.getResource(1); std::cout << std::endl; 
		std::cout << "r.getResource(2) : "; r.getResource(2); std::cout << std::endl; 
		std::cout << "r.getResource(3) : "; r.getResource(3); std::cout << std::endl; 
	}

	catch (const char* c) {
		std::cout << c << std::endl;
	}

	catch (...) {
		std::cout << "Exception caught\n";
	}

	return 0;
}
