#include <iostream>
#include <stdexcept>

class MyExcept : public std::runtime_error {
	public:
		MyExcept(const char* str) : std::runtime_error(str) {}
};

void fun() {
	throw MyExcept("Runtime Error in fun");
}

int main() {
	try {
		fun();
	}

	catch (MyExcept& e) {
		std::cout << e.what() << std::endl;
	}

	catch (exception& e) {
		std::cout << e.what() << std::endl;
	}

	catch (...) {
		std::cout << "Exception caught\n";
	}

	return 0;
}
