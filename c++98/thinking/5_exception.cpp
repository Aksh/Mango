#include <iostream>

class test {
	private:
		static int counter;
		int id;
	public:
		test() {
			id = counter++;
			std::cout << "Constructing test #" << id << std::endl;
			if (id == 3) throw 3;
		}

		~test() {
			std::cout << "Destructing test #" << id << std::endl;
		}
};

int test::counter = 0;

int main() {
	try {
		test t1;
		test arr[4];
		test t6;
	}
	catch (...) {
		std::cout << "Exception caught\n";
	}

	return 0;
}
