#include <iostream>

class base {
	public:
		class baseExcept {};
		base(int i) {
			throw baseExcept();
		}
};

class derived : public base {
	public:
		class derivedExcept {
			const char* msg;
			public:
				derivedExcept(const char* str) : msg(str) {}
				const char* what() const {return msg;}
		};

		derived(int j) try : base(j) {
			std::cout << "This will never print\n";
		}

		catch (baseExcept& be) {
			throw derivedExcept("base subobject threw");
		}
};

int main() {
	try {
		derived d(1);
	}

	catch (derived::derivedExcept& de) {
		std::cout << de.what() << std::endl;
	}

	return 0;
}
