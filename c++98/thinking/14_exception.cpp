#include <iostream>
#include <new>
#include <exception>
#include <cstring>

class hasPointers {
	struct myData {
		const char* theString;
		const int* theInt;
		size_t numInts;
		myData(const char* otherString, const int* otherInt, size_t nInts) : 
				theString(otherString), theInt(otherInt), numInts(nInts) {}
	}*theData;

	static myData* clone(const char* otherString, const int* otherInt, size_t nInts) {
		char* newChars = new char[strlen(otherString) + 1];
		int* newInts;
		try {
			newInts = new int[nInts];
		}
		catch (std::bad_alloc&) {
			delete[] newChars;
			throw;
		}
		try {
			strcpy(newChars, otherString);
			for (size_t i = 0; i < nInts; ++i)
				newInts[i] = otherInt[i];
		}
		catch (...) {
			delete[] newChars;
			delete[] newInts;
			throw;
		}

		return new myData(newChars, newInts, nInts);
	}

	static myData* clone(const myData* otherData) {
		return clone(otherData->theString,
					 otherData->theInt,
					 otherData->numInts);
	}

	static void cleanup(const myData* theData) {
		delete[] theData->theString;
		delete[] theData->theInt;
		delete theData;
	}

	public:
		hasPointers(const char* someString, const int* someInts, size_t someNum) {
			theData = clone(someString, someInts, someNum);
		}

		hasPointers(const hasPointers& source) {
			theData = clone(source.theData);
		}

		hasPointers& operator=(const hasPointers& rhs) {
			if (this != &rhs) {
				myData* newData;
				newData = clone(rhs.theData->theString, rhs.theData->theInt, rhs.theData->numInts);
				cleanup(theData);
				theData = newData;
			}

			return *this;
		}

		~hasPointers() {
			cleanup(theData);
		}

		friend std::ostream& operator<<(std::ostream& os, const hasPointers& obj) {
			os << obj.theData->theString << " : ";
			for (size_t i = 0; i < obj.theData->numInts; ++i)
				os << obj.theData->theInt[i] << ' ';

			return os;
		}
};

int main() {
	int someInts[] = {1, 2, 3, 4};
	size_t someCount = sizeof someInts / sizeof someInts[0];
	int someMoreInts[] = {5, 6, 7};
	size_t someMoreCount = sizeof someMoreInts / sizeof someMoreInts[0];
	hasPointers h1("Hello", someInts, someCount);
	hasPointers h2("Good Bye", someMoreInts, someMoreCount);
	std::cout << "H1 : " << h1 << std::endl;
	std::cout << "H2 : " << h2 << std::endl;
	std::cout << "H1 = H2\n";
	h1 = h2;
	std::cout << "H1 : " << h1 << std::endl;
	return 0;
}
