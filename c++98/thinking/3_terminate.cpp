#include <iostream>
#include <exception>

class Error {
	const std::string msg;
	public:
		Error(std::string str) : msg(str) {}
		const std::string
		get_error() {
			return msg;
		}
};

void fun() {
	throw Error("something goes wrong");
}

void gun()
{
	std::cout << "My custom terminate() function\n";
	exit(0);
}

int main() {
	try {
		void (*ptr)();
		ptr = std::set_terminate(gun);
		fun();
		std::set_terminate(ptr);
	}
	catch (std::string s) {
			std::cout << s << std::endl;
	}
	//catch (Error& e) {
	//	std::cout << e.get_error() << std::endl;
	//}
	return 0;
}
