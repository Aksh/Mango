#include "stack.h"
#include <fstream>

using namespace std;
int main(int argc, char* argv[])
{
	ifstream in(argv[1]);
	Stack textlines;
	string line;
	// Read file and store lines in the stack:
	while(getline(in, line))
		textlines.push(new string(line));
	// Pop the lines from the stack and print them:
	string* s;
	while((s = (string*)textlines.pop()) != 0) {
		cout << *s << endl;
		delete s;
	}
}
