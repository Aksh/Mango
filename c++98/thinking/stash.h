#ifndef _H_STASH_H__
#define _H_STASH_H__

#include <iostream>

class Stash {
	int quantity;
	int size;
	int next;
	unsigned char* storage;

	void inflate(int increase);
public:
	Stash(int size);
	Stash(int size, int initQuantity);
	~Stash();
	int add(void* element);
	void* fetch(int index);
	int count();
};

#endif // _H_STASH_H__
