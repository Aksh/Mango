#include <iostream>

class Error {
	const std::string msg;
	public:
		Error(std::string str) : msg(str) {}
		const std::string
		get_error() {
			return msg;
		}
};

void fun() {
	throw Error("something goes wrong");
}

int main() {
	try {
		fun();
	}
	catch (std::string s) {
			std::cout << s << std::endl;
	}
	catch (Error& e) {
		std::cout << e.get_error() << std::endl;
	}
	return 0;
}
