#include <iostream>

class resource {
	public:
		resource() {std::cout << "resource::resource()\n";}
		~resource() {std::cout << "resource::~resource()\n";}
};

class useResource {
	resource* r;
	public:
		useResource(int i) {
			std::cout << "useResource::useResource()\n";
			r = new resource[i];
			throw 4;
		}
		
		~useResource() {
			std::cout << "useResource::~useResource()\n";
			delete []r;
		}
};

int main() {
	try {
		useResource r(3);
	}
	catch (...) {
		std::cout << "Exception caught\n";
	}

	return 0;
}
