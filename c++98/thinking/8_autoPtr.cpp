#include <iostream>
#include <memory>

class test {
	public:
		test() {std::cout << "test::test()\n";}
		~test() {std::cout << "test::~test()\n";}
		void fun() {std::cout << "test::fun()\n";}
};

int main() {
	test* t = new test();
	std::auto_ptr<test> ptr(t);
	ptr->fun();
	//delete t;  // should not free the object as it will be free by auto_ptr template class
	return 0;
}
