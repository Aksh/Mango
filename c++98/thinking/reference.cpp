#include <iostream>

int main()
{
	int x = 0;
	int& a = x;
	a++;
	std::clog << "x : " << x << std::endl;
	const int& y = 12;
	//int& y = 12;
	//y++;
	std::clog << "y : " << y << std::endl;
	return 0;
}
