#include <iostream>
#include <cstdlib>

class demo {
	public:
		demo() {
			std::clog << "demo::constrctor()\n";
		}

		~demo() {
			std::clog << "demo::destructor()\n";
			throw 3;
		}
};

void fun()
{
	demo d;
	throw 3;
}

void gun()
{
	std::clog << "inside custom terminator\n";
	exit(0);
}

int main()
{
	std::set_terminate(gun);
	try {
		fun();
	}
	catch (int i) {
		std::clog << "exception occure\n";
	}

	return 0;
}
