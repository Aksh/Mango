#include <iostream>

class base {
	public:
		class baseExcept {};
		class derivedExcpt : public baseExcept {};
		virtual void fun() throw (baseExcept) {
			throw baseExcept();
		}
};

class derived : public base {
	public:
		//void fun() throw (baseExcept) {
		void fun() throw (derivedExcpt) {
			//throw derivedExcpt();
			throw baseExcept();
		}
};

void terminate() {
	std::cout << "My custom terminate()\n";
}

void unexpected() {
	std::cout << "My custom unexpected()\n";
	//throw base::derivedExcpt();
}

int main() {
	std::set_terminate(terminate);
	std::set_unexpected(unexpected);
	try {
		derived d;
		d.fun();
	}
	catch (base::derivedExcpt de) {
		std::cout << "derived exception : \n" << std::endl;
	}
	catch (base::baseExcept be) {
		std::cout << "base exception : \n" << std::endl;
	}

	std::cout << "main exting\n";

	return 0;
}
