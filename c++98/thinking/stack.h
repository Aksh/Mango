#ifndef _H_STACK_H__
#define _H_STACK_H__

#include <iostream>

class Stack {
	struct Link {
		void* data;
		Link* next;
		Link(void*, Link*);
		~Link();
	}* head;

public:
	Stack();
	~Stack();
	void push(void*);
	void* peak();
	void* pop();
};

#endif // _H_STACK_H__
