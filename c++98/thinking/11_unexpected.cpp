#include <iostream>
#include <exception>

class Full {};
class Empty {};

void gun() {
	throw 47;
}

//void fun(int i) throw (Full, Empty) {
//void fun(int i) throw () {
void fun(int i) throw (Full, Empty){
	switch (i) {
		case 1 : throw Full();
		case 2 : throw Empty();
	}

	gun();
}

void myUnexpected() {
	std::cout << "unexpected exception caught\n";
	exit(0);
}

int main() {
	std::set_unexpected(myUnexpected);
	for (int i = 1; i <= 3; ++i) {
		try {
			fun(i);
		}

		catch (Full& f) {std::cout << "Caugh Full exception\n";}
		catch (Empty& e) {std::cout << "Caugh Empty exception\n";}
		catch (...) {std::cout << "Caugh unknown exception\n";}
	}

	return 0;
}
