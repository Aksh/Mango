#include <iostream>

class Error {
	const std::string msg;
	public:
		Error(std::string str) : msg(str) {}
		const std::string
		get_error() {
			return msg;
		}
};

void fun() {
	//throw Error("something goes wrong");
	throw "string exception";
}

void gun() {
	try {
		fun();
	}
	catch (...) {
		//
		// do some cleanup
		//
		std::cout << "Cleaning up...\n";
		throw;
	}
}

int main() {
	try {
		gun();
	}
	catch (const char* s) {
			std::cout << s << std::endl;
	}
	catch (Error& e) {
		std::cout << e.get_error() << std::endl;
	}
	catch (...) {
		std::cout << "Unknown exception\n";
	}
	return 0;
}
