#include "stack.h"

Stack::Link::Link(void* dat, Link* lnk)
{
	data = dat;
	next = lnk;
}

Stack::Link::~Link()
{ }

Stack::Stack()
{
	head = 0;
}

void Stack::push(void* dt)
{
	head = new Link(dt, head);
}

void* Stack::peak()
{
	if (head) {
		return head->data;
	}

	return NULL;
}

void* Stack::pop()
{
	if (!head)
		return NULL;

	void* result = head->data;
	Link* oldLink = head;
	head = head->next;
	delete oldLink;
	return result;
}

Stack::~Stack()
{
	while (head)
		pop();
}
