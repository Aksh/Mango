#include "stash.h"

const int increament = 100;

Stash::Stash(int sz)
{
	size = sz;
	quantity =0;
	next = 0;
	storage = 0;
}

Stash::Stash(int sz, int initQuantity)
{
	size = sz;
	quantity =0;
	next = 0;
	storage = 0;
	inflate(initQuantity);
}

int Stash::add(void* element)
{
	if (next >= quantity)
		inflate(increament);
	// Copy element into storage
	// starting at next empty space
	int startByte = next * size;
	unsigned char* e = (unsigned char *) element;
	for (int i = 0; i < size; i++)
		storage[startByte + i] = e[i];
	next++;
	return (next - 1);
}

void* Stash::fetch(int index)
{
	if (index > next)
		return 0;
	return &(storage[index * size]);
}

int Stash::count()
{
	return next;
}

void Stash::inflate(int increase)
{
	int newQuantity = quantity + increase;
	int newBytes = newQuantity * size;
	int oldBytes = quantity * size;
	unsigned char* b = new unsigned char[newBytes];
	for (int i = 0; i < oldBytes; i++)
		b[i] = storage[i];
	delete []storage;
	storage = b;
	quantity = newQuantity;
}

Stash::~Stash()
{
	if (storage) {
		delete []storage;
	}
}
