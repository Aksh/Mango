#include <iostream>

class demo {
	int ii;
	public:
		demo(int i) {
			std::clog << "demo() : " << i << "\n";
			ii = i;
		}

		~demo() {
			std::clog << "~demo() : " << ii << "\n";
		}
};

void fun()
{
	demo d2(2);
	throw 2;
	demo d3(3);
}

int main()
{
	demo d6(6);
	try {
		demo d1(1);
		fun();
		demo d4(4);
	}
	catch (int i) {
		std::clog << "exception occured\n";
	}
	demo d5(5);
	return 0;
}
