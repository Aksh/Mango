#include <iostream>
#include <exception>

class Error {
	const std::string msg;
	public:
		Error(std::string str) : msg(str) {}
		const std::string
		get_error() {
			return msg;
		}
};

class test {
	public:
	void fun() {
		throw Error("something goes wrong");
	}

	~test() {
		std::cout << "Inside destructor of ~test()\n";
		//
		// throwing an exception from destructor is the sign of poor design
		//
		throw 'a';
	}
};

void gun()
{
	std::cout << "My custom terminate() function\n";
	exit(0);
}

int main() {
	try {
		void (*ptr)() = std::set_terminate(gun);
		test t;
		t.fun();
		std::set_terminate(ptr);
	}
	catch (std::string s) {
		std::cout << s << std::endl;
	}
	catch (Error& e) {
		std::cout << e.get_error() << std::endl;
	}
	catch (...) {
		std::cout << "Unknown exception\n";
	}
	return 0;
}
