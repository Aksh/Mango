#include <iostream>
#include <exception>
#include <cstdlib>

void terminate() {
	std::cout << "My custom terminate()\n";
	exit(0);
}

void unexpected() {
	std::cout << "My custom unexpected()\n";
	std::cout << "Throwing int exception\n";
	throw 1;
}

void fun() throw (std::bad_exception) {
//void fun() throw (int) {
//void fun() throw (char, int) {
	std::cout << "Throwing char exception\n";
	throw 'a';
}

int main() {
	std::set_terminate(terminate);
	std::set_unexpected(unexpected);
	try {
		fun();
	}
	catch (char c) {
		std::cout << "char exception : " << c << std::endl;
	}
	catch (int i) {
		std::cout << "int exception : " << i << std::endl;
	}
	catch (...) {
		std::cout << "bad_exception\n";
	}

	std::cout << "main function exiting\n";
	return 0;
}
