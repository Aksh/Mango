void fun(char);

int main()
{
	fun(11); // linker error, linker can not find definition for char argument as it was told explicitely to compiler
	return 0;
}
