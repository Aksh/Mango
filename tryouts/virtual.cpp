#include <iostream>

class base {
	public:
		virtual void fun() {
			std::cout << "base::fun()\n";
		}
};

class der : public base {
	public:
		void fun() {
			std::cout << "der::fun()\n";
		}
};

int main()
{
	der d;
	d.fun();
	base b;
	b.fun();
	b = d;
	b.fun();
	return 0;
}
