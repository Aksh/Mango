#include <iostream>

class superUnion {
	enum {
		character,
		integer,
		floating
	}varType;

	union {
		int i;
		char c;
		float f;
	};

	public:

	superUnion(int ii) {
		varType = integer;
		i = ii;
	}

	superUnion(char cc) {
		varType = character;
		c = cc;
	}

	superUnion(float ff) {
		varType = floating;
		f = ff;
	}

	void print() {
		switch (varType) {
			case integer:
				std::clog << i << std::endl;
				break;
			case character:
				std::clog << c << std::endl;
				break;
			case floating:
				std::clog << f << std::endl;
				break;
		}
	}
};

int main()
{
	superUnion A('c'), B(12), C(1.44F);
	A.print();
	B.print();
	C.print();
	return 0;
}
