#include <iostream>

union u {
	int i;
	float f;
	u(int ii) {i = ii;}
	u(float ff) {f = ff;}
	~u() {}
	int read_int() {return i;}
	float read_float() {return f;}
};

int main()
{
	u ui(11), uf(3.24f);
	std::clog << ui.read_int() << std::endl;
	std::clog << uf.read_float() << std::endl;
	//std::clog << ui.read_float() << std::endl;
	return 0;
}
