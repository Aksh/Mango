#include <iostream>

void fun(int i, int j = 10)
{
	std::clog << "Inside default fun\n";
}

void fun(int i)
{
	std::clog << "Inside non-default fun\n";
}

int main()
{
	fun(11, 12);
	//fun(11); 		//error, ambiguity occurs
	return 0;
}
