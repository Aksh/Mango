#include <iostream>

class demo {
	public:
		demo() {}
		demo(const demo& d) {
			std::cout << "inside demo copy constructor\n";
		}

		demo& operator =(const demo& rhs) {
			std::cout << "inside assignmnt operator\n";
		}
};

int main()
{
	demo d;
	demo d1;
	d = d1;
	return 0;
}
